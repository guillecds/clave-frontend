import { Component, OnInit } from '@angular/core';
import { CitasService } from 'src/app/services/citas/citas.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { ProfesionalesService } from 'src/app/services/profesionales/profesionales.service';
import { Profesional } from 'src/app/models/profesional';
import { ChartDataSets, ChartType, ChartOptions, Chart } from 'chart.js';
import { MatTableDataSource } from '@angular/material/table';
import { DatePipe } from '@angular/common';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-estadisticas',
  templateUrl: './estadisticas.component.html',
  styleUrls: ['./estadisticas.component.css'],
  providers: [DatePipe]
})
export class EstadisticasComponent implements OnInit {

  // Gráfico de barras 
  barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{
      ticks: {
        fontColor: "black"
    }
    }], yAxes: [{ticks: {
      fontColor: "black",
      beginAtZero: true
  }}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    },
    legend: {
      display: true,      
      labels: {                    
       fontColor: 'black'
      }
     }
  };

  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartData: ChartDataSets[] = [];

  public barChartColors: any [] = [
    {backgroundColor:'#00b3ff'},
    {backgroundColor:'#a98fe9'},
    {backgroundColor:'#ed1c23'},
    {backgroundColor:'#ff8b55'},
    {backgroundColor:'#131313'}
  ];
  
  // Gráfico de torta 
  chart: Chart;
  atendidas: number = 0;
  canceladas: number = 0;
  anuladas: number = 0;
  pendientes: number = 0;
  ausentes: number = 0;
  
  // Variables  
  token: string;
  identidad: any;
  statusResponse: any;
  flag: boolean = false;
  selectedProfesional: any = "Seleccione Profesional";
  profesionales: Profesional[];
  year: number;

  constructor(private _usuariosService: UsuarioService, private _citasService: CitasService, private _profesionalesService: ProfesionalesService) { 
    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();
    this._profesionalesService.getProfesionales(this.token).subscribe(
      Response => {
        this.statusResponse = Response.status;
        if (this.statusResponse === 'success') {
          this.profesionales = Response.data;
        } 
      },
      error => {
        console.log(error);
      });
  }

  ngOnInit() {
  }

  getCitasEstadisticas(){
    this._citasService.getCitaEstadistica(this.token, this.selectedProfesional.id).subscribe(
      Response => {
        this.statusResponse = Response.status;
        this.flag = true;
        if (this.statusResponse === 'success') {
          this.year = Response.year;
          this.atendidas = 0;
          this.canceladas = 0;
          this.ausentes = 0;
          this.anuladas = 0;
          this.pendientes = 0;
          this.barChartLabels = [];

          let auxAtendidas = [];
          let auxCanceladas = [];
          let auxAusentes = [];
          let auxAnuladas = [];
          let auxPendientes = [];

          Response.lastYear.forEach(element => {
            this.barChartLabels.push(element.mes);
            auxAtendidas.push(element.atendidas);
            auxPendientes.push(element.pendientes);
            auxAusentes.push(element.ausentes);
            auxCanceladas.push(element.canceladas);
            auxAnuladas.push(element.anuladas);
          }); 

          this.barChartData = [
            { data: auxAtendidas, label: 'Citas Atendidas' },
            { data: auxCanceladas, label: 'Citas Canceladas' },
            { data: auxAnuladas, label: 'Citas Anuladas' },
            { data: auxPendientes, label: 'Citas Pendientes' },
            { data: auxAusentes, label: 'Citas Ausentes' }
          ];

          Response.lastMonth.forEach(element => {
            if (element.estado == "ATENDIDA")
              this.atendidas ++;
            if (element.estado == "CANCELADA")
              this.canceladas ++;
            if (element.estado == "AUSENTE")
              this.ausentes ++;
            if (element.estado == "ANULADA")                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
              this.anuladas ++;
            if (element.estado == "PENDIENTE")
              this.pendientes ++;
          });          

          if (typeof this.chart !== "undefined") {
            this.chart.destroy();
          }
          this.chart = new Chart('torta', {
            type: 'doughnut',          
            data: {
                    labels: ['Atendidas', 'Canceladas', 'Anuladas', 'Pendientes', 'Ausentes'],
                    datasets: [{
                      label: 'Citas',
                      fill: false,
                      data: [Number(this.atendidas.toFixed()), Number(this.canceladas.toFixed()), Number(this.anuladas.toFixed()), Number(this.pendientes.toFixed()), Number(this.ausentes.toFixed())],
                      backgroundColor: ['#00b3ff','#a98fe9','#ed1c23', '#ff8b55', '#131313'],
                      hoverBackgroundColor: ['#66c5ee','#baaddb','#db4449', '#e79772', '#302e2e']
                    }]
            },
            options: {
                responsive: true,                  
                circumference: 1 * Math.PI,
                rotation: 1 * Math.PI,
                tooltips: {
                  enabled: true
                },
                legend: {
                  display: true,
                  position: 'left',
                  labels: {                    
                    fontColor: 'black'
                  }
                }
              }
          });
        } 
      },
      error => {
        console.log(error);
      });
  }

}
