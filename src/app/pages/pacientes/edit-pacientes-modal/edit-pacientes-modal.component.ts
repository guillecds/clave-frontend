import { Component, Inject, OnInit } from '@angular/core';
import { Paciente } from 'src/app/models/paciente';
import { UsuarioService } from 'src/app/services/services.index';
import { ObrasSocialesService } from 'src/app/services/obrassociales/obrassociales.service';
import { Usuario } from 'src/app/models/usuario';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-edit-pacientes-modal',
  templateUrl: './edit-pacientes-modal.component.html',
  styleUrls: ['./edit-pacientes-modal.component.css']
})
export class EditPacientesModalComponent implements OnInit {

  paciente: Paciente;
  sexo: any;
  tipodoc: any;
  token: string;  
  identidad: Usuario;
  obrassociales: any;
  provincias: string[];
  selectedObraSocial: any;
  constructor(private _usuariosService: UsuarioService, private _obrassocialesService: ObrasSocialesService, @Inject(MAT_DIALOG_DATA) public data: any,private _pacientesService: PacientesService) {
    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();
  
    this.sexo= ['Mujer', 'Hombre', 'Otro'];
    this.tipodoc=['DNI', 'LC', 'LE', 'Otro'];
    this.provincias=['SAN JUAN','BUENOS AIRES','CATAMARCA','CHACO','CHUBUT','CÓRDOBA','CORRIENTES','ENTRE RÍOS','FORMOSA','JUJUY','LA PAMPA','LA RIOJA',
    'MENDOZA', 'MISIONES','NEUQUÉN','RÍO NEGRO','SALTA','SAN LUIS','SANTA CRUZ','SANTA FE','SANTIAGO DEL ESTERO','TIERRA DEL FUEGO','TUCUMÁN'];
    this.paciente=data.element;
    this.selectedObraSocial = data.element.obrasocial;
    // if(this.paciente.obrasocial==0)
    // { 
    //   this.paciente.obrasocial=null;
    // }
  
    this._obrassocialesService.getObrasSociales(this.token).subscribe(
      Response => {
        if (Response.status=== 'success') 
          this.obrassociales= Response.data;
      },
      error => {
        console.log(error);
      });


   }

  ngOnInit() { }

  OnSubmit()
  {
    // console.log(this.paciente);
    this.paciente.obrasocial=this.selectedObraSocial.id;
    this._pacientesService.updatePaciente(this.token, this.paciente).subscribe(
      Response => {
       
        if (Response.status === 'success') {
      
            Swal.fire('Paciente actualizado', 'El paciente fue actualizado de forma exitosa', 'success');
         
        }
        else if(Response.status== 'error')
        {
          console.log(Response.message);
        }
    },
      error => {
        Swal.fire('Error', error.statusText, 'error');
      })
  }



}
