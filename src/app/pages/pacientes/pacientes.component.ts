import { Component,  ViewChild, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, MatSortable } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UsuarioService } from 'src/app/services/services.index';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import { Usuario } from 'src/app/models/usuario';
import { ModalHistoryComponent } from '../lista/modalhistory/modalhistory.component';
import { EditPacientesModalComponent } from '../pacientes/edit-pacientes-modal/edit-pacientes-modal.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { AddPacientesModalComponent } from './add-pacientes-modal/add-pacientes-modal.component';
import { Paciente } from 'src/app/models/paciente';

@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.css']
})
export class PacientesComponent implements OnInit {
  
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>([]);
  displayedColumns: string[] = ['apynom', 'doc', 'ficha', 'tel','historia', 'opciones'];


  token: string;  
  identidad: Usuario;
  pacientes: any;
  itemPerPage = 20;
  constructor(private _usuariosService: UsuarioService, private _pacientesService: PacientesService, public dialog: MatDialog, ) { 
    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();

    this._pacientesService.getAllPacientes(this.token).subscribe(
      Response => {
     
        if (Response.status === 'success') {
          this.dataSource = new MatTableDataSource(Response.data);
          this.pacientes=Response.data;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          } 
      },
      error => {
        console.log(error);
      });
  }
  
  ngOnInit() {}
  
  applyFilter(filterValue: string) 
  {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.filterPredicate = function(data, filter: string): boolean 
    { return data.apynom.toLowerCase().includes(filter) || data.doc.toString().includes(filter)};  
  }

  openDialogHistory(element: any): void {
    console.log('element',element);

    this.dialog.open(ModalHistoryComponent, {
    disableClose: false,
    minWidth: "35%",
    minHeight: "35%",
    data: {
      element:element,
      idpac: element.id
          }
   });
  }

  borrarPaciente(element:any)
  {
    Swal.fire(
      {
        title: '¿Estás seguro?',
        type: 'warning',
        text: 'El paciente será eliminado del sistema',
        showConfirmButton: true,
        confirmButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        showCancelButton: true,
      }
    ).then ((result) => {
    if (result.value) {
      this._pacientesService.destroy(this.token, element).subscribe(
        Response => {
          if (Response.status === 'success') {
        
            Swal.fire('Éxito', 'El paciente ha sido borrado de forma exitosa', 'success');
            for (let i = 0; i < this.dataSource.data.length; i++) {
              if (this.dataSource.data[i].id === Response.paciente.id) {
                   this.dataSource.data.splice(i, 1);
                    this.dataSource._updateChangeSubscription();
              }
            }
          } else {
            Swal.fire('Error', Response.message, 'error');
          }
        },
        error => {
          console.log(error);
          Swal.fire('Error', 'Ups! Algo salio mal!', 'error');
        });
    }
 });
  }

  addPaciente(): void
  { 
    let dialogRef = this.dialog.open(AddPacientesModalComponent, {
      disableClose: false,
      width: "40%",
      height: "95%"
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        window.location.reload();
        // this.pacientes.push(result);
        // this.pacientes.sort = this.sort;
        // this.dataSource = new MatTableDataSource(this.pacientes);
        // this.dataSource.paginator = this.paginator;
        // this.dataSource.sort = this.sort;
        // this.dataSource._updateChangeSubscription();
      }
    });
  }

  editarPaciente(element: any) :void
  {
    let dialogRef = this.dialog.open(EditPacientesModalComponent, {
      disableClose: false,
      width: "auto",
      height: "95%",
      data: {
        element:element
            }
    });
  }



}
