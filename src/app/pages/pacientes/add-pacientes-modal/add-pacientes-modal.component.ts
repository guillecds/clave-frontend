import { Component, Inject, OnInit } from '@angular/core';

import { Paciente } from 'src/app/models/paciente';
import { UsuarioService } from 'src/app/services/services.index';
import { ObrasSocialesService } from 'src/app/services/obrassociales/obrassociales.service';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';

import { Usuario } from 'src/app/models/usuario';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-add-pacientes-modal',
  templateUrl: './add-pacientes-modal.component.html',
  styleUrls: ['./add-pacientes-modal.component.css']
})
export class AddPacientesModalComponent implements OnInit {

  
  paciente: Paciente;
  sexo: any;
  tipodoc: any;
  token: string;  
  identidad: Usuario;
  obrassociales: any;
  provincias: string[];
  selectedObraSocial: any=null;

  constructor(private _usuariosService: UsuarioService, private _obrassocialesService: ObrasSocialesService, private _pacientesService: PacientesService,public dialogRef: MatDialogRef < AddPacientesModalComponent >, public dialog:MatDialog ) { 
    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();
    this.paciente= new Paciente(null, null,null, null, null, null, null, 
      null, null, null, null, null,null, null, null, null);
    this.sexo= ['Mujer', 'Hombre', 'Otro'];
    this.tipodoc=['DNI', 'LC', 'LE', 'Otro'];
    this.provincias=['SAN JUAN','BUENOS AIRES','CATAMARCA','CHACO','CHUBUT','CÓRDOBA','CORRIENTES','ENTRE RÍOS','FORMOSA','JUJUY','LA PAMPA','LA RIOJA',
    'MENDOZA', 'MISIONES','NEUQUÉN','RÍO NEGRO','SALTA','SAN LUIS','SANTA CRUZ','SANTA FE','SANTIAGO DEL ESTERO','TIERRA DEL FUEGO','TUCUMÁN'];
    this._obrassocialesService.getObrasSociales(this.token).subscribe(
      Response => {
        if (Response.status === 'success') {
        this.obrassociales= Response.data;
          } 
      },
      error => {
  
        console.log(error);
      });
  }

  ngOnInit() {}

  OnSubmit()
  {

    if(this.selectedObraSocial==null || this.paciente.apynom==null || this.paciente.sexo==null  || this.paciente.tipodoc==null  || this.paciente.doc==null  ||this.paciente.nac==null || this.paciente.prov==null )
    {
      Swal.fire('Error', 'Debe cargar todos los datos requeridos', 'error');
    } else 
      {
      this.paciente.obrasocial=this.selectedObraSocial.id;
      this._pacientesService.guardarPaciente(this.token, this.paciente).subscribe(
        Response => {
          if (Response.status === 'success') 
          {
              Swal.fire('Paciente cargado', 'El paciente fue cargado de forma exitosa', 'success');
              this.dialogRef.close(Response.data);
          }
          // IMPROVE
          else if(Response.status == 'error')
          {
            console.log(Response.message);
          }
      },
        error => 
        {
          Swal.fire('Error', error.statusText, 'error');
          this.dialog.closeAll();
        })
    }
  
  }

}
