import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ModalEstadoComponent } from './dashboard/modalestado/modalestado.component';
import { ModalDetailsComponent } from './lista/modaldetails/modaldetails.component';
import { ModalHistoryComponent } from './lista/modalhistory/modalhistory.component';
import { EditPacientesModalComponent } from './pacientes/edit-pacientes-modal/edit-pacientes-modal.component';

import { ModalespecialidadComponent } from './especialidades/modalespecialidad/modalespecialidad/modalespecialidad.component';

import { ProgressComponent } from './progress/progress.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { EditarUsuarioComponent } from './usuarios/editar-usuario/editar-usuario.component';
import { EditarPerfilComponent } from './usuarios/editar-perfil/editar-perfil.component';
import { ListaComponent } from './lista/lista.component';
import { CalendarioComponent } from './calendario/calendario.component';
import { MensajesComponent } from './mensajes/mensajes.component';
import { AdministracionComponent } from './administracion/administracion.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { PacientesComponent } from './pacientes/pacientes.component';
import { ProfesionalesComponent } from './profesionales/profesionales.component';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';
import { EspecialidadesComponent } from './especialidades/especialidades.component';
import { ObrasSocialesComponent } from './obrassociales/obrassociales.component';
import { ModalAddCitaComponent } from './lista/modalAddCita/modalAddCita.component';
import { AddPacientesModalComponent } from './pacientes/add-pacientes-modal/add-pacientes-modal.component';
import { AddProfesionalModalComponent } from './profesionales/add-profesional-modal/add-profesional-modal.component';
import { EditProfesionalModalComponent } from './profesionales/edit-profesional-modal/edit-profesional-modal.component';
import { ObraSocialModalComponent } from './obrassociales/obra-social-modal/obra-social-modal.component';
import { ProfConsultoriosModalComponent } from './profesionales/prof-consultorios-modal/prof-consultorios-modal.component';
import { HorariosModalComponent } from './profesionales/horarios-modal/horarios-modal.component';
import { UsersAdmComponent } from './usersadm/usersadm.component';
import { AddUsersModalComponent } from './usersadm/add-users-modal/add-users-modal.component';
import { EditUsersModalComponent } from './usersadm/edit-users-modal/edit-users-modal.component';
import { RelacionesComponent } from './relaciones/relaciones.component';
import { ModalAddRelComponent } from './relaciones/modalAddRel/modalAddRel.component';
import { ModalAtencionProfesionalComponent } from './profesionales/modal-atencion-profesional/modal-atencion-profesional.component';

const pagesRoutes: Routes = [
    {
        path: 'pages',
        component: PagesComponent,
        children: [
           { path: 'dashboard', component: DashboardComponent, data: { titulo: 'Dashboard' } },
           { path: 'app-modal-estado', component: ModalEstadoComponent, data: { titulo: 'Cambiar Estado Cita' } },
           { path: 'app-modal-details', component: ModalDetailsComponent, data: { titulo: 'Ver detalles' } },
           { path: 'app-modal-history', component: ModalHistoryComponent, data: { titulo: 'Ver historia clínica' } },
           { path: 'app-modal-editpacientes', component: EditPacientesModalComponent , data: { titulo: 'Editar Pacientes' } },
           { path: 'app-modal-addpacientes', component: AddPacientesModalComponent , data: { titulo: 'Agregar Pacientes' } },
           { path: 'app-modal-especialidades', component: ModalespecialidadComponent, data: { titulo: 'Editar Especialidad' } },
           { path: 'app-modal-addprofesionales', component: AddProfesionalModalComponent, data: { titulo: 'Agregar Profesional' } },
           { path: 'app-modal-editprofesionales', component: EditProfesionalModalComponent, data: { titulo: 'Editar Profesional' } },
           { path: 'app-modal-profconsultoriosmodal', component: ProfConsultoriosModalComponent, data: { titulo: 'Ver Consultorios' } },
           { path: 'app-modal-modalatencionprofesional', component: ModalAtencionProfesionalComponent, data: { titulo: 'Ver Atencion' } },
           { path: 'app-modal-horariosmodal', component: HorariosModalComponent, data: { titulo: 'Ver Horarios' } },
           { path: 'app-modal-obrassociales', component: ObraSocialModalComponent, data: { titulo: 'Agregar Obra Social' } },
           { path: 'app-modal-addusers', component: AddUsersModalComponent, data: { titulo: 'Agregar Usuario' } },
           { path: 'app-modal-editusers', component: EditUsersModalComponent, data: { titulo: 'Editar Usuario' } },
           { path: 'app-modal-addcita', component: ModalAddCitaComponent , data: { titulo: 'Añadir cita' } },
           { path: 'app-modal-addrel', component: ModalAddRelComponent, data: { titulo: 'Añadir Relación' } },
           { path: 'progress', component: ProgressComponent, data: { titulo: 'Progress' } },
           { path: 'lista', component: ListaComponent, data: { titulo: 'Lista de Atención' } },
           { path: 'calendario', component: CalendarioComponent, data: { titulo: 'Calendario' } },
           { path: 'mensajes', component: MensajesComponent, data: { titulo: 'Mensajes' } },
           { path: 'administracion', component: AdministracionComponent, data: { titulo: 'Administracion' } },
           { path: 'configuracion', component: ConfiguracionComponent, data: { titulo: 'Configuracion' } },
           { path: 'pacientes', component: PacientesComponent, data: { titulo: 'Pacientes' } },
           { path: 'profesionales', component: ProfesionalesComponent, data: { titulo: 'Profesionales' } },
           { path: 'estadisticas', component: EstadisticasComponent, data: { titulo: 'Estadisticas' } },
           { path: 'especialidades', component: EspecialidadesComponent, data: { titulo: 'Especialidades' } },
           { path: 'obrassociales', component: ObrasSocialesComponent, data: { titulo: 'Obras Sociales' } },
           { path: 'usersadm', component: UsersAdmComponent, data: { titulo: 'Usuarios' } },

      
           // USUARIOS
           { path: 'usuarios', component: UsuariosComponent, data : {titulo: 'Crear usuario'}},
           { path: 'editar-usuario/:idUsuario', component: EditarUsuarioComponent, data : {titulo: 'Cuenta de Usuario'}},
           { path: 'editar-perfil/:idUsuario', component: EditarPerfilComponent, data : {titulo: 'Editar Perfil'}},
           { path: 'relaciones', component: RelacionesComponent, data: { titulo: 'Relaciones' } },
          
           // LOGOUT
           { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
        ]
   }
];

export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );
