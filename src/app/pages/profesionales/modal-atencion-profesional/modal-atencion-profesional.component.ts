import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Profesional } from 'src/app/models/profesional';
import { Usuario } from 'src/app/models/usuario';
import { ProfesionalesService } from 'src/app/services/profesionales/profesionales.service';
import { UsuarioService } from 'src/app/services/services.index';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-atencion-profesional',
  templateUrl: './modal-atencion-profesional.component.html',
  styleUrls: ['./modal-atencion-profesional.component.css']
})
export class ModalAtencionProfesionalComponent implements OnInit {

  
  token: string;  
  identidad: Usuario;
  statusResponse: string;
  profesional: Profesional;
  constructor(private _usuariosService: UsuarioService,private _profesionalesService: ProfesionalesService, @Inject(MAT_DIALOG_DATA) public data: any) {

      this.token = _usuariosService.getToken();
      this.identidad = _usuariosService.getIdentidad();
      this.profesional= data.element;
  
  
  
   }

  ngOnInit() {
    // this._profesionalesService.getAtencionProfesional(this.token, this.data.element.id).subscribe(
    //   Response => {
    //       this.statusResponse = Response.status;

    //       if (this.statusResponse === 'success') {
      
    //         console.log(Response.profesional);
    //         this.profesional = Response.profesional;
         
    //       }
    //   },
    //   error => {
  
    //     console.log(error);
    //   });
  }

  onSubmit()
  {
 
        this._profesionalesService.updateProfesional(this.token, this.profesional).subscribe(
    Response => {
      this.statusResponse = Response.status;

      if (this.statusResponse === 'success') {
        
          Swal.fire('Horarios de atención actualizados', 'Los tipos de turnos fueron actualizados de forma exitosa', 'success');
       
      }
  },
    error => {
      Swal.fire('Error', error.statusText, 'error');
    })

  }

}
