import { Component, Inject, OnInit } from '@angular/core';
import { MatTableDataSource, MAT_DIALOG_DATA } from '@angular/material';
import { Consultorio } from 'src/app/models/consultorio';
import { Usuario } from 'src/app/models/usuario';
import { ConsultoriosService } from 'src/app/services/consultorios/consultorios.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-prof-consultorios-modal',
  templateUrl: './prof-consultorios-modal.component.html',
  styleUrls: ['./prof-consultorios-modal.component.css']
})
export class ProfConsultoriosModalComponent implements OnInit {

  token: string;  
  identidad: Usuario;
  statusResponse: string;
  flagAdd: boolean = false;
  newConsultorio: Consultorio = {
    id: null,
    idprof: null,
    domicilio: '',
    localidad: '',
    provincia: '',
    telefono: '',
    nombre: '',
    notas: ''
  };
  dataSource = new MatTableDataSource<any>([]);
  displayedColumns: string[] = ['domicilio', 'localidad','provincia' ,'telefono','nombre', 'notas', 'opciones'];

  constructor(private _usuariosService: UsuarioService, @Inject(MAT_DIALOG_DATA) public data: any,private _consultoriosService: ConsultoriosService) {
    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();
   
    this._consultoriosService.getConsultoriosProfesional(this.token, data.element.id).subscribe(
      Response => {
          this.statusResponse = Response.status;
          if (this.statusResponse === 'success') {
            this.dataSource = new MatTableDataSource(Response.data);
          }
      },
      error => {
        console.log(error);
      });
   }

  ngOnInit() {
  }

  addConsultorio(){
    this.newConsultorio.idprof = this.data.element.id;
      this._consultoriosService.guardarConsultorio(this.token, this.newConsultorio).subscribe(
      Response => 
      {
        if (Response.status === 'success') 
        {
            Swal.fire('Consultorio cargado', 'El consultorio fue cargado de forma exitosa', 'success');
        }
    },
      error => 
      {
        Swal.fire('Error', error.statusText, 'error');
      })
  }

  editarConsultorio(element: any) {

  }

  borrarConsultorio(element: any) {

    
    Swal.fire(
      {
        title: '¿Estás seguro?',
        type: 'warning',
        text: 'El consultorio será eliminado del sistema',
        showConfirmButton: true,
        confirmButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        showCancelButton: true,
      }
    ).then ((result) => {
    if (result.value) {
      this._consultoriosService.destroy(this.token, element).subscribe(
        Response => {
          if (Response.status === 'success') {
        
            Swal.fire('Éxito', 'El consultorio ha sido borrado de forma exitosa', 'success');

            for (let i = 0; i < this.dataSource.data.length; i++) { //TO-DO: mejorar esto
              if (this.dataSource.data[i].id === Response.consultorio.id) {//verifico que el id del paciente sea igual al id del paciente que se esta eliminando
                   this.dataSource.data.splice(i, 1);
                    this.dataSource._updateChangeSubscription();
              }
            }
          } else {
            Swal.fire('Error', Response.message, 'error');
          }
        },
        error => {
          console.log(error);
          Swal.fire('Error', 'Ups! Algo salio mal!', 'error');
        });
    }
 });

  }

  

}
