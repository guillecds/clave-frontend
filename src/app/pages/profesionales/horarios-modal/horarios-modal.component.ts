import { Component, Inject, OnInit } from '@angular/core';
import { MatTableDataSource, MAT_DIALOG_DATA } from '@angular/material';
import { Usuario } from 'src/app/models/usuario';
import { HorariosService } from 'src/app/services/horarios/horarios.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-horarios-modal',
  templateUrl: './horarios-modal.component.html',
  styleUrls: ['./horarios-modal.component.css']
})
export class HorariosModalComponent implements OnInit {
  
  token: string;  
  identidad: Usuario;
  statusResponse: string;
 
  
  dataSource = new MatTableDataSource<any>([]);
  displayedColumns: string[] = ['dia', 'horaini','horafin' ,'domicilio', 'opciones'];

  
  constructor(private _usuariosService: UsuarioService, @Inject(MAT_DIALOG_DATA) public data: any,private _horariosService: HorariosService) 
  {

    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();

    this._horariosService.getHorariosProfesional(this.token, data.element.id).subscribe(
      Response => {
          this.statusResponse = Response.status;

        if (this.statusResponse === 'success') {
          this.dataSource = new MatTableDataSource(Response.data);
  
          } 
      },
      error => {
  
        console.log(error);
      });

  }

  deleteHorario(element: any)
  {
   
    Swal.fire(
      {
        title: '¿Estás seguro?',
        type: 'warning',
        text: 'El horario será eliminado del sistema',
        showConfirmButton: true,
        confirmButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        showCancelButton: true,
      }
    ).then ((result) => {
    if (result.value) {
      this._horariosService.destroy(this.token, element).subscribe(
        Response => {
          if (Response.status === 'success') {
        
            Swal.fire('Éxito', 'El horario ha sido borrado de forma exitosa', 'success');

            for (let i = 0; i < this.dataSource.data.length; i++) {
              if (this.dataSource.data[i].id === Response.horario.id) {
                   this.dataSource.data.splice(i, 1);
                    this.dataSource._updateChangeSubscription();
              }
            }
          } else {
            Swal.fire('Error', Response.message, 'error');
          }
        },
        error => {
          console.log(error);
          Swal.fire('Error', 'Ups! Algo salio mal!', 'error');
        });
    }
 });
  }

  addHorario()
  {

  }

  updateHorario(element: any)
  {

  }

  

  



  ngOnInit() {
  }

}
