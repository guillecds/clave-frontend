import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';

//Models
import { Profesional } from 'src/app/models/profesional';
import { Usuario } from 'src/app/models/usuario';

//Services
import { EspecialidadesService } from 'src/app/services/especialidades/especialidades.service';
import { ProfesionalesService } from 'src/app/services/profesionales/profesionales.service';
import { UsuarioService } from 'src/app/services/services.index';

@Component({
  selector: 'app-add-profesional-modal',
  templateUrl: './add-profesional-modal.component.html',
  styleUrls: ['./add-profesional-modal.component.css']
})
export class AddProfesionalModalComponent implements OnInit {

  
  token: string;  
  identidad: Usuario;
  profesional:Profesional;
  tipodoc=['DNI', 'LC', 'LE', 'Otro'];
  especialidades: any;
  selectedEspecialidad:any = null;
  provincias: string[];
  constructor(private _usuariosService: UsuarioService,private _especialidadesService: EspecialidadesService,private _profesionalesService: ProfesionalesService) 
  {
    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();
    this.profesional= new Profesional(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
    this.provincias=['SAN JUAN','BUENOS AIRES','CATAMARCA','CHACO','CHUBUT','CÓRDOBA','CORRIENTES','ENTRE RÍOS','FORMOSA','JUJUY','LA PAMPA','LA RIOJA',
    'MENDOZA', 'MISIONES','NEUQUÉN','RÍO NEGRO','SALTA','SAN LUIS','SANTA CRUZ','SANTA FE','SANTIAGO DEL ESTERO','TIERRA DEL FUEGO','TUCUMÁN'];
    this._especialidadesService.getEspecialidades(this.token).subscribe(
      Response => {    
        if (Response.status === 'success') {
            this.especialidades= Response.data;
            //TO-DO - borrar este console
            console.log(this.especialidades);
          } 
      },
      error => 
      {
        console.log(error);
      });


   }

  ngOnInit() {  }

  OnSubmit()
  {
  // TODO
    //ELIMINAR CITAMINUTOS, CITACNOHORA Y CONFIG, XQ SON REQUIRED EN LA BDD PERO HABRÍA Q CAMBIARLO Y ACÁ NO HACE FALTA (PARECE)
    console.log(this.profesional);
    if(this.profesional.apynom == null || this.profesional.tipodoc == null|| this.profesional.doc == null|| this.profesional.matricula == null
      || this.selectedEspecialidad== null || this.profesional.tel== null || this.profesional.prov== null || this.profesional.mail == null/*|| this.profesional.citaminutos== null || this.profesional.citaconhora== null || this.profesional.config == null*/)
    {
      Swal.fire('Error', 'Debe cargar todos los datos requeridos', 'error');

    }
    else 
    {
      this.profesional.especialidad=this.selectedEspecialidad.id;
      this._profesionalesService.guardarProfesional(this.token, this.profesional).subscribe(
      Response => 
      {
        if (Response.status === 'success') 
        {
            Swal.fire('Profesional cargado', 'El profesional fue cargado de forma exitosa', 'success');
        }
    },
      error => 
      {
        Swal.fire('Error', error.statusText, 'error');
      })
    }
   
  }

}
