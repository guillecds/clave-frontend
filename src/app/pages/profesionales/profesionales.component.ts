import { Component, OnInit,ViewChild } from '@angular/core';


import { MatPaginator } from '@angular/material/paginator';
import { MatSort, MatSortable } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Usuario } from 'src/app/models/usuario';
import { ProfesionalesService } from 'src/app/services/profesionales/profesionales.service';
import { UsuarioService } from 'src/app/services/services.index';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { AddProfesionalModalComponent } from './add-profesional-modal/add-profesional-modal.component';
import { EditProfesionalModalComponent } from './edit-profesional-modal/edit-profesional-modal.component';
import { ProfConsultoriosModalComponent } from './prof-consultorios-modal/prof-consultorios-modal.component';
import { HorariosModalComponent } from './horarios-modal/horarios-modal.component';
import { ModalAtencionProfesionalComponent } from './modal-atencion-profesional/modal-atencion-profesional.component';

@Component({
  selector: 'app-profesionales',
  templateUrl: './profesionales.component.html',
  styleUrls: ['./profesionales.component.css']
})
export class ProfesionalesComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  dataSource = new MatTableDataSource<any>([]);
  displayedColumns: string[] = ['apynom', 'tel','cel' ,'especialidad','consultorio', 'opciones'];

  token: string;  
  identidad: Usuario;
  statusResponse: string;
  pacientes: any;
  itemPerPage = 20;
  
  constructor(private _usuariosService: UsuarioService, private _profesionalesService: ProfesionalesService, public dialog: MatDialog) {
    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();

    this._profesionalesService.getProfesionalesFull(this.token).subscribe(
      Response => {
        this.statusResponse = Response.status;

        if (this.statusResponse === 'success') {
          this.dataSource = new MatTableDataSource(Response.data);
          console.log(Response.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
     
        } 
      },
      error => {
  
        console.log(error);
      });

   }

  ngOnInit() {
  }

  addProfesional(): void
  {
  
    let dialog = this.dialog.open(AddProfesionalModalComponent, {
      disableClose: false,
      width: "40%",
      height: "95%"
    });

    dialog.afterClosed().subscribe(result => {
      if(result){
        window.location.reload();
      }
    });


  }

  
  editarProfesional(element: any) :void
  {
    let dialog = this.dialog.open(EditProfesionalModalComponent, {
      disableClose: false,
      width: "40%",
      height: "95%",
      data: {
        element:element
            }
    });
  }

  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.filterPredicate = function(data, filter: string): boolean {
      return   data.apynom.toLowerCase().includes(filter)        
  };  
  }

  

  borrarProfesional(element:any)
  {
    Swal.fire(
      {
        title: '¿Estás seguro?',
        type: 'warning',
        text: 'El profesional será eliminado del sistema',
        showConfirmButton: true,
        confirmButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        showCancelButton: true,
      }
    ).then ((result) => {
    if (result.value) {
      this._profesionalesService.destroy(this.token, element).subscribe(
        Response => {
          if (Response.status === 'success') {
        
            Swal.fire('Éxito', 'El profesional ha sido borrado de forma exitosa', 'success');

            for (let i = 0; i < this.dataSource.data.length; i++) {
              if (this.dataSource.data[i].id === Response.paciente.id) {
                   this.dataSource.data.splice(i, 1);
                    this.dataSource._updateChangeSubscription();
              }
            }
          } else {
            Swal.fire('Error', Response.message, 'error');
          }
        },
        error => {
          console.log(error);
          Swal.fire('Error', 'Ups! Algo salio mal!', 'error');
        });
    }
 });
  }

  verConsultorios(element: any)
  {
    
    let dialog = this.dialog.open(ProfConsultoriosModalComponent , {
      disableClose: false,
      minWidth: "600px",
      minHeight: "350px",
      data: {
                element: element
            }
    });
  }


  
  verAtencion(element: any)
  {
    
    let dialog = this.dialog.open(ModalAtencionProfesionalComponent , {
      disableClose: false,
      minWidth: "600px",
      minHeight: "350px",
      data: {
                element: element
            }
    });
  }


  verHorarios(element: any)
  {
    
    let dialog = this.dialog.open(HorariosModalComponent , {
      disableClose: false,
      minWidth: "800px",
      minHeight: "400px",
      data: {
              element: element
            }
    });
  }

}
