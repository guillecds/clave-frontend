import { Component, OnInit, Inject } from '@angular/core';
import { Profesional } from 'src/app/models/profesional';
import { Usuario } from 'src/app/models/usuario';
import Swal from 'sweetalert2';
import { ProfesionalesService } from 'src/app/services/profesionales/profesionales.service';
import { UsuarioService } from 'src/app/services/services.index';
import { EspecialidadesService } from 'src/app/services/especialidades/especialidades.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-edit-profesional-modal',
  templateUrl: './edit-profesional-modal.component.html',
  styleUrls: ['./edit-profesional-modal.component.css']
})
export class EditProfesionalModalComponent implements OnInit {

  
  token: string;  
  identidad: Usuario;
  profesional:Profesional;
  tipodoc=['DNI', 'LC', 'LE', 'Otro'];
  provincias=['SAN JUAN','BUENOS AIRES','CATAMARCA','CHACO','CHUBUT','CÓRDOBA','CORRIENTES','ENTRE RÍOS','FORMOSA','JUJUY','LA PAMPA','LA RIOJA',
    'MENDOZA', 'MISIONES','NEUQUÉN','RÍO NEGRO','SALTA','SAN LUIS','SANTA CRUZ','SANTA FE','SANTIAGO DEL ESTERO','TIERRA DEL FUEGO','TUCUMÁN'];
  especialidades: any;
  selectedEspecialidad:any;
  constructor(private _usuariosService: UsuarioService,private _especialidadesService: EspecialidadesService,@Inject(MAT_DIALOG_DATA) public data: any, private _profesionalesService: ProfesionalesService) 
  {
    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();
    this.profesional=data.element;
    this.selectedEspecialidad = data.element.especialidad;
    // console.log(this.profesional);
    this._especialidadesService.getEspecialidades(this.token).subscribe(
      Response => {
        if (Response.status === 'success') {
            this.especialidades= Response.data;
            console.log(this.especialidades);
          } 
      },
      error => {
  
        console.log(error);
      });

  }

  ngOnInit() {
  }

  OnSubmit()
  {
      this.profesional.especialidad=this.selectedEspecialidad.id;
 
          this._profesionalesService.updateProfesional(this.token, this.profesional).subscribe(
      Response => {
    
        if (Response.status === 'success') {
      
            Swal.fire('Profesional actualizado', 'El profesional fue actualizado de forma exitosa', 'success');
         
        }
    },
      error => {
        Swal.fire('Error', error.statusText, 'error');
      })
  }

}
