import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ModalEstadoComponent } from './dashboard/modalestado/modalestado.component';
import { ModalDetailsComponent } from './lista/modaldetails/modaldetails.component';
import { ModalHistoryComponent } from './lista/modalhistory/modalhistory.component';
import { EditPacientesModalComponent } from './pacientes/edit-pacientes-modal/edit-pacientes-modal.component';



import { ProgressComponent } from './progress/progress.component';
import { PagesComponent } from './pages.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

// Modules
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { StickyModule } from 'ng2-sticky-kit';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ComponentsModule } from '../components/components.module';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { ScrollingModule } from '@angular/cdk/scrolling';
import {NgxPaginationModule} from 'ngx-pagination';
import { FilterPipeModule } from 'ngx-filter-pipe';

// Graficos
import { ChartsModule } from 'ng2-charts';

// Rutas
import { PAGES_ROUTES } from './pages.routes';
// Pipes
import { NewPercentPipe } from '../pipes/new-percent.pipe';
import { NoimagePipe } from '../pipes/noimage.pipe';
import { ThousandsPipe } from '../pipes/thousands.pipe';
// temporal
import { IncrementadorComponent } from '../components/incrementador/incrementador.component';

// tslint:disable-next-line:quotemark
import { DataTableModule } from "angular-6-datatable";

import { DomseguroPipe } from '../pipes/domseguro.pipe';
import { LoadingComponent } from './loading/loading.component';

import { AddCommentComponent } from '../components/add-comment/add-comment.component';

import { UsuariosComponent } from './usuarios/usuarios.component';
import { EditarUsuarioComponent } from './usuarios/editar-usuario/editar-usuario.component';
import { EditarPerfilComponent } from './usuarios/editar-perfil/editar-perfil.component';


import { SlickCarouselModule } from 'ngx-slick-carousel';
import { SnackbarComponent } from '../components/snackbar/snackbar.component';
import { Ng5SliderModule } from 'ng5-slider';
import { HttpClientModule } from '@angular/common/http';

import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { ListaComponent } from './lista/lista.component';
import { CalendarioComponent } from './calendario/calendario.component';
import { MensajesComponent } from './mensajes/mensajes.component';
import { AdministracionComponent } from './administracion/administracion.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { PacientesComponent } from './pacientes/pacientes.component';
import { ProfesionalesComponent } from './profesionales/profesionales.component';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';
import { EspecialidadesComponent } from './especialidades/especialidades.component';
import { ObrasSocialesComponent } from './obrassociales/obrassociales.component';
import { ModalespecialidadComponent } from './especialidades/modalespecialidad/modalespecialidad/modalespecialidad.component';
import { ModalAddCitaComponent } from './lista/modalAddCita/modalAddCita.component';
import { AddPacientesModalComponent } from './pacientes/add-pacientes-modal/add-pacientes-modal.component';
import { AddProfesionalModalComponent } from './profesionales/add-profesional-modal/add-profesional-modal.component';
import { EditProfesionalModalComponent } from './profesionales/edit-profesional-modal/edit-profesional-modal.component';
import { ProfConsultoriosModalComponent } from './profesionales/prof-consultorios-modal/prof-consultorios-modal.component';
import { ObraSocialModalComponent } from './obrassociales/obra-social-modal/obra-social-modal.component';
import { HorariosModalComponent } from './profesionales/horarios-modal/horarios-modal.component';
import { AddUsersModalComponent } from './usersadm/add-users-modal/add-users-modal.component';
import { EditUsersModalComponent } from './usersadm/edit-users-modal/edit-users-modal.component';
import { UsersAdmComponent } from './usersadm/usersadm.component';
import { ModalAtencionProfesionalComponent } from './profesionales/modal-atencion-profesional/modal-atencion-profesional.component';
import { RelacionesComponent } from './relaciones/relaciones.component';
import { ModalAddRelComponent } from './relaciones/modalAddRel/modalAddRel.component';

@NgModule({

    
    declarations: [
        PagesComponent,
        DashboardComponent,
        ModalEstadoComponent,
        ModalDetailsComponent,
        ModalHistoryComponent,
        ModalAddCitaComponent,
        EditPacientesModalComponent,
        ModalespecialidadComponent,
        ProgressComponent,
        IncrementadorComponent,
        NoimagePipe,
  
        DomseguroPipe,
        LoadingComponent,
        NewPercentPipe,
        ThousandsPipe,
        
        UsuariosComponent,
        EditarUsuarioComponent,
        EditarPerfilComponent,
        ListaComponent,
        CalendarioComponent,
        MensajesComponent,
        AdministracionComponent,
        ConfiguracionComponent,
        PacientesComponent,
        EditPacientesModalComponent,
        ProfesionalesComponent,
        EstadisticasComponent,
        EspecialidadesComponent,
        ObrasSocialesComponent,
        ModalespecialidadComponent,
        ModalespecialidadComponent,
        AddPacientesModalComponent,
        AddProfesionalModalComponent,
        EditProfesionalModalComponent,
        ProfConsultoriosModalComponent,
        HorariosModalComponent,
        ObraSocialModalComponent,
        HorariosModalComponent,
        AddUsersModalComponent,
        EditUsersModalComponent,
        ModalAtencionProfesionalComponent,
        UsersAdmComponent,
        ModalAddRelComponent,
        RelacionesComponent
        
    ],
    exports: [
        PagesComponent,
        DashboardComponent,
        ProgressComponent,
    ],
    imports: [
        AngularMaterialModule,
        SharedModule,
        PAGES_ROUTES,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        NgSelectModule,
        DataTableModule,
        BrowserAnimationsModule,
        StickyModule,
        PdfViewerModule,
        ComponentsModule,
        ChartsModule,
        ScrollingModule,
        NgxPaginationModule,
        FilterPipeModule,
        Ng5SliderModule,
        SlickCarouselModule,
        HttpClientModule,
        NgProgressModule,
        NgProgressHttpModule
    ],
    entryComponents: [
        AddCommentComponent,
        SnackbarComponent
    ]

})

export class PagesModule { }
