import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
// Servicios
import { UsuarioService } from '../../../services/usuario/usuario.service';
// Componentes
import { Router } from '@angular/router';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import Swal from 'sweetalert2';
import { Paciente } from 'src/app/models/paciente';
import { ObrasSocialesService } from 'src/app/services/obrassociales/obrassociales.service';
import { ModalEstadoComponent } from '../../dashboard/modalestado/modalestado.component';

@Component({
  selector: 'app-modal-details',
  templateUrl: './modaldetails.component.html',
  styleUrls: ['./modaldetails.component.css']
})
export class ModalDetailsComponent implements OnInit {

  token: string;
  dataSource= new MatTableDataSource<any>([]);
  flagEdit: boolean = false;
  statusResponse: string;
  paciente: any;
  obrassociales: any;
  selectedOS: any;
  sexo: any[] = ['Mujer', 'Hombre', 'Otro'];

  constructor(private _formBuilder: FormBuilder, 
              @Inject(MAT_DIALOG_DATA) public data: any,
              private _userService: UsuarioService,
              private _pacienteService: PacientesService,
              private _obrassocialesService: ObrasSocialesService, 
              private renderer: Renderer2, 
              public dialog: MatDialog,
              public dialogRef: MatDialogRef<ModalDetailsComponent>, 
              public router: Router) {
      this.token = this._userService.getToken();
  }

  ngOnInit() {
console.log(this.data.element);
    if(this.data.element.sexo ==1)
      this.data.element.sexo = 'Hombre';
    else
      this.data.element.sexo = 'Mujer';
    
    this._obrassocialesService.getObrasSociales(this.token).subscribe(
      Response => {
        if (Response.status=== 'success') 
          this.obrassociales= Response.data;
      },
      error => {
        console.log(error);
      }
    );
    
    setTimeout(() => {
      this.obrassociales.forEach(element => {
        if(element.id == this.data.element.obrasocial){
          this.data.element.obrasocial = element.nombre;
        }
      });
    }, 1500);
  }

  close() {
    this.dialogRef.close();
  }

  changeCitaState(): void {

    let dialog = this.dialog.open(ModalEstadoComponent, {
      disableClose: false,
      minHeight: "500px",
      data: {
              idCita: this.data.element.id
            }
    });
    
    dialog.afterClosed().subscribe(result => {
      if(result) {
        this.dialogRef.close();
      }
    });  
  }

  savePaciente(){

    this.paciente = this.data.element;
    this.paciente.id = this.data.element.idpac;
    
    let aux = this.paciente.obrasocial.id;
    this.paciente.nameobrasocial = this.paciente.obrasocial.nombre;
    this.paciente.obrasocial = aux;

    this._pacienteService.updatePaciente(this.token, this.paciente).subscribe(
      Response => {
        if (Response.status === 'success') 
            Swal.fire('Paciente actualizado', 'El paciente fue actualizado de forma exitosa', 'success');
        else if(Response.status== 'error')
          console.log(Response.message);
      },
      error => {
        Swal.fire('Error', error.statusText, 'error');
      })
    this.flagEdit = false;
  }

}
