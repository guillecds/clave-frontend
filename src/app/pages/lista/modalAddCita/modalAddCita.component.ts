import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';

// Servicios
import { UsuarioService } from '../../../services/usuario/usuario.service';
import { CitasService } from 'src/app/services/citas/citas.service';
import { APIURL } from 'src/app/services/apiUrl';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Paciente } from 'src/app/models/paciente';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import { ObrasSocialesService } from 'src/app/services/obrassociales/obrassociales.service';

@Component({
  selector: 'app-modal-addcita',
  templateUrl: './modalAddCita.component.html',
  styleUrls: ['./modalAddCita.component.css']
})
export class ModalAddCitaComponent implements OnInit {

  // Usuario
  token: string;

  dataSource= new MatTableDataSource<any>([]);

  statusResponse: string;
  horario: any;
  cita: any;
  paciente: any = {
    doc: '',
    apynom: '',
    sexo: '...',
    tel: '',
    cel: '',
    direcc: '',
    loc: '',
    prov: '',
    nac: '',
    obrasocial: 0,
  };
  selectedTipo: any;
  selectedOS: any;
  notas: any = '';
  tipoCita: any = [
    {tipo: 'Consulta'},
    {tipo: 'Estudio'},
    {tipo: 'Tratamiento'},
    {tipo: 'Control'},
    {tipo: 'Práctica'},
    {tipo: 'Prioritaria'},
    {tipo: 'No dar turno'},
    {tipo: 'Sobreturno'}
  ];
  sexo: any = [
    {tipo: 'Hombre'},
    {tipo: 'Mujer'},
    {tipo: 'Otro'}
  ];
  selectedSex: any;
  selectOS: any;

  constructor(private _formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any,
              private _userService: UsuarioService, private renderer: Renderer2, public dialog: MatDialog,
              public dialogRef: MatDialogRef<ModalAddCitaComponent>, public router: Router, private _citasService: CitasService, private _pacienteService: PacientesService, private _obrasService: ObrasSocialesService) {
      
    this.token = this._userService.getToken();
    
  }

  ngOnInit() {
    this.horario = new Date(this.data.hora + 10800000);
    this.horario = this.horario.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
    console.log(this.horario);
    
    this._obrasService.getObrasSociales(this.token).subscribe(
      Response => {
        if(Response.status === 'success'){
          this.selectOS = Response.data;
        } else {
          Swal.fire('Error', 'Hubo un problema al obtener las obras sociales', 'error');
        }

      } 
    )

  }

  close() {
    this.dialogRef.close();
  }

  buscarPac() {
    this._pacienteService.pacientexDoc(this.token, this.paciente.doc).subscribe(
      Response => {

        if(Response.status === 'success'){

          this.paciente = Response.paciente;
          this.selectOS.forEach(element => {
            if(element.id == this.paciente.obrasocial)
              this.selectedOS = element.nombre;
          });
          
        } else {
          Swal.fire('Error', 'Hubo un problema al obtener el paciente', 'error');
        }

      } 
    )
    
  }

  saveCita(valid) {
    //Ordenar la cita
    this.data.notas = this.notas;
    this.data.tipocita = this.selectedTipo.tipo;
    this.paciente.sexo = this.selectedSex.tipo;
    this.paciente.obrasocial = this.selectedOS.id;
    console.log('test', this.data);
    console.log('test1', this.paciente);
    this._citasService.addCita(this.token, this.data, this.paciente).subscribe(
      Response => {
        this.statusResponse = Response.status;
        if(this.statusResponse === 'success'){
          Swal.fire('Exito', 'La cita fue guardada correctamente', 'success');
          this.dialogRef.close();
        } else {
          Swal.fire('Error', 'Hubo un problema al guardar su cita', 'error');
        }

      } 
    )
  
  }

}
