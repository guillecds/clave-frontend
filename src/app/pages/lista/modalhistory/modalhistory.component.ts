import { Component, OnInit, Inject, Renderer2, ViewChild, ElementRef } from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { APIURL } from 'src/app/services/apiUrl';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
// import { SelectionModel } from '@angular/cdk/collections';

// Servicios
import { UsuarioService } from '../../../services/usuario/usuario.service';
import { HistoriasService } from 'src/app/services/historias/historias.service';
import { AntecedentesService } from 'src/app/services/antecedentes/antecedentes.service';
import { MatSort } from '@angular/material/sort';

//Models
import { Antecedentes } from 'src/app/models/antecedentes';
import { HistoriaClinica } from 'src/app/models/historiaclinica';
import { CargaImagenService } from 'src/app/services/cargaImagen/carga-imagen.service';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';

@Component({
  selector: 'app-modal-history',
  templateUrl: './modalhistory.component.html',
  styleUrls: ['./modalhistory.component.css']
})
export class ModalHistoryComponent implements OnInit {

  // Usuario
  token: string;
  f = new Date();
  fecha: any;
  day: Date;
  estudios: any;
  historias: any;
  flagHistorias: boolean = false;
  flagFiles: boolean = false;
  historia: HistoriaClinica;
  antecedentes: Antecedentes;
  tmpDate: any;
  displayedColumns: string[] = ['archivo', 'opciones'];
  dataSource= new MatTableDataSource<any>([]);
  sort:any;
  @ViewChild(MatSort, {static: false}) set content(content: ElementRef)
  {
    this.sort=content;
    if(this.sort)
    {
      this.dataSource.sort=this.sort;
    }
  }

  constructor(private _formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any,
              private _userService: UsuarioService, private _pacientesService: PacientesService, private _uploadService: CargaImagenService, private renderer: Renderer2, public dialog: MatDialog,
              public dialogRef: MatDialogRef<ModalHistoryComponent>, public router: Router, private _historiasService: HistoriasService, private _antecedentesService: AntecedentesService) 
  {
    this.token = this._userService.getToken();
    this.historia = new HistoriaClinica(null,null,null,null,null,null,null);
    this.antecedentes= new Antecedentes(null, null, null, null, null, null, null);
    //Arreglar fecha
    this.tmpDate = new Date().getTime();
    

  }


  ngOnInit() 
  {
    this.fecha= this.f.getDate() + "-"+ (this.f.getMonth()+1) + "-" +this.f.getFullYear();
    //Get historias del paciente (Anteriores)
    this._historiasService.getHistorias(this.token,this.data.element.id).subscribe(
      Response => {
        if(Response.status=== 'success'){
          this.historias = Response.data;
          this.flagHistorias = true;
          if(this.historias.length  == 0) this.flagHistorias = false;
        }
      },
      error => { console.log(error);
      });


    //Get antecedentes del paciente 

    this._antecedentesService.getAntecedentes(this.token,this.data.idpac).subscribe
    (
      Response => {
        if(Response.status === 'success')
        {
          this.antecedentes = Response.antecedentes;
          console.log(this.antecedentes);
        }
      },
      error => { console.log(error);}
    );

    this._pacientesService.estudiosxPaciente(this.token,this.data.idpac).subscribe
    (
      Response => {
        if(Response.status === 'success')
        {
          this.dataSource=new MatTableDataSource(Response.estudios);
          this.dataSource.sort = this.sort;
          this.flagFiles = true;
        }
      },
      error => { console.log(error);}
    );

    this._pacientesService.getHcInfo(this.token,this.data.idpac).subscribe
    (
      Response => {
        if(Response.status === 'success')
        {
          console.log('recibí esto', Response);


          // this.dataSource=new MatTableDataSource(Response.estudios);
          // this.dataSource.sort = this.sort;
          // this.flagFiles = true;
        }
      },
      error => { console.log(error);}
    );
  }

  guardarHistoria()
  {
    this.historia.idpac= this.data.element.id;
    this.historia.fecha = this.tmpDate;
    this._historiasService.storeHistoria(this.token,this.historia).subscribe
    (
      Response => {
        if (Response.status === 'success') 
        {
          this.antecedentes.idpac = this.data.element.id;
          this.antecedentes.creacion= this.tmpDate.toString();
          this.antecedentes.modificado= this.tmpDate.toString();

          this._antecedentesService.updateAntecedentes(this.token,this.antecedentes).subscribe(
            Response => {
              if (Response.status === 'success') 
                Swal.fire('Historia cargada', 'La historia fue agregada de forma exitosa', 'success');
            },
            error => { Swal.fire('Error', error.statusText, 'error');}
            )
            this.dialogRef.close();
        }
    },
      error => { Swal.fire('Error', error.statusText, 'error');}
    )
    this.dialogRef.close();
  }
  
  guardarAntecedente()
  {
    // ARREGLAR CREACION Y MODIFICADO
    this.antecedentes.idpac = this.data.element.id;
    this.antecedentes.creacion= this.tmpDate.toString();
    this.antecedentes.modificado= this.tmpDate.toString();

    this._antecedentesService.updateAntecedentes(this.token,this.antecedentes).subscribe
    (
      Response => {
        if (Response.status === 'success') 
        {
          Swal.fire('Antecedente actualizado', 'El antecedente fue actualizado de forma exitosa', 'success');
        }
    },
      error => { Swal.fire('Error', error.statusText, 'error');}
    )
    this.dialogRef.close();
  }

  downloadFile(file){
    window.open(APIURL.urlDownload + file, '_blank');
  }

  saveFile(){
    (async () => {

      const { value: file } = await Swal.fire({
        title: 'Cargar archivo',
        input: 'file',
        inputAttributes: {
          'accept': '*',
          'aria-label': 'Seleccionar'
        }
      })
      
      if (file) {
        const reader = new FileReader()
        reader.onload = (e) => {
          console.log(e);
          console.log(file);
        }
        reader.readAsDataURL(file)
        this._uploadService.subirImagen(file, this.data.idpac, null).subscribe
        (
          Response => {
            if (Response.status === 'success') 
            {
              Swal.fire('Estudio cargado', 'El estudio fue subido exitosamente al servidor', 'success');
            }
        },
          error => { Swal.fire('Error', error.statusText, 'error');}
        );
      }
      
      })()
  }

}
