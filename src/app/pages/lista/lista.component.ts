import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

//Models
import { Usuario } from 'src/app/models/usuario';
import { Profesional } from 'src/app/models/profesional';
import { Cita } from 'src/app/models/cita';

//Services 
import { ProfesionalesService } from 'src/app/services/profesionales/profesionales.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { CitasService } from 'src/app/services/citas/citas.service';

import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { ModalDetailsComponent } from './modaldetails/modaldetails.component';
import { ModalHistoryComponent } from './modalhistory/modalhistory.component';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})

export class ListaComponent implements OnInit {
  ngOnInit(): void {
  }

  sort:any;
  @ViewChild(MatSort, {static: false}) set content(content: ElementRef)
  {
    this.sort=content;
    if(this.sort)
    {
      this.dataSource.sort=this.sort;
    }
  }
  displayedColumns: string[] = ['paciente', 'dni', 'detalles', 'opciones'];

  dataSource= new MatTableDataSource<any>([]);
  token: string;
  identidad: Usuario;
  statusResponse: string;
  profesionales: any[];
  citas: Cita[];
  selectedProfesional: any = "Seleccione Profesional";
  flag: boolean = false



  constructor(private _usuariosService: UsuarioService, private _profesionalesService: ProfesionalesService, private _citasService: CitasService, public dialog: MatDialog) {

    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();
    if (this.identidad.rol == 'AD') {
      this._profesionalesService.getProfesionales(this.token).subscribe(
        Response => {
          this.statusResponse = Response.status;
  
          if (this.statusResponse === 'success') {
            this.profesionales = Response.data;
     
          } 
        },
        error => {
          console.log(error);
        });
    } else if (this.identidad.rol == 'GS') {
      this._profesionalesService.getProfesionalId(this.token, this.identidad.idprof).subscribe(
        Response => {
          this.statusResponse = Response.status;
          if (this.statusResponse === 'success') {
            this.selectedProfesional = Response.data;
            this.getCitas();
          } 
        },
        error => {
          console.log(error);
        });
    } else if (this.identidad.rol == 'AS') {
      this._usuariosService.getRelList(this.token, this.identidad.sub).subscribe(
        Response => {
          this.statusResponse = Response.status;
          if (this.statusResponse === 'success') {
            this.profesionales = Response.data;
            this.profesionales.forEach(element => {
              element.id = element.idprof;
            })
          } 
        },
        error => {
          console.log(error);
        });
    }
  }

  onDelete() {
    this.flag=false;
  }
  openDetails(element: any)
  {
    this.openDialogDetails(element);
  }

  
  openDialogDetails(element: any): void {
  
    let dialog = this.dialog.open(ModalDetailsComponent, {
     disableClose: false,
     maxHeight: "45em",
     minWidth: "400px",
     data: {
           element:element
           }
   });
   //asd

   
 }

  openDialogHistory(element: any): void {
    this.dialog.open(ModalHistoryComponent, {
    disableClose: false,
    minWidth: "5%",
    data: {
      element:element,
      idpac: element.idpac
            }
    });
    this.dataSource._updateChangeSubscription();
  }

  openHistory(element: any)
  {
    this.openDialogHistory(element);
    
  }

  changeState(idcita: number, idatencion: number)
  {
    this._citasService.updateStateAtencion(this.token, idcita,idatencion).subscribe(

      Response => {
  
        this.statusResponse = Response.status;
        if(this.statusResponse === 'success')
        {
          console.log('test');
          this.refreshTable(idcita);
        }
      } 
    )
  }

  refreshTable(id: number)
  {
    console.log('test', id);
    for (let i = 0; i < this.dataSource.data.length; i++) {
      if (this.dataSource.data[i].id === id){
        this.dataSource.data.splice(i, 1);
        this.dataSource._updateChangeSubscription();
      }
    }
  }

  getCitas() {
    this._citasService.getAtencionPorProfesional(this.token, this.selectedProfesional.id).subscribe(
      Response => {
        this.statusResponse = Response.status;
        if (this.statusResponse === 'success') {
          console.log(Response.data);
          this.flag=true;
          this.citas = Response.data;
          this.dataSource=new MatTableDataSource(Response.data);
          this.dataSource.sort = this.sort;
        } 
      },
      error => {
        this.flag=false;
        console.log(error);
      });
  }



}
