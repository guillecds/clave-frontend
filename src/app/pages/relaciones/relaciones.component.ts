import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

//Models
import { Usuario } from 'src/app/models/usuario';
import { Profesional } from 'src/app/models/profesional';
import { Cita } from 'src/app/models/cita';

//Services 
import { ProfesionalesService } from 'src/app/services/profesionales/profesionales.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { CitasService } from 'src/app/services/citas/citas.service';

import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { ModalAddRelComponent } from './modalAddRel/modalAddRel.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-relaciones',
  templateUrl: './relaciones.component.html',
  styleUrls: ['./relaciones.component.css']
})

export class RelacionesComponent implements OnInit {
  ngOnInit(): void {
  }

  sort:any;
  @ViewChild(MatSort, {static: false}) set content(content: ElementRef)
  {
    this.sort=content;
    if(this.sort)
    {
      this.dataSource.sort=this.sort;
    }
  }
  displayedColumns: string[] = ['profesional', 'opciones'];

  dataSource= new MatTableDataSource<any>([]);
  token: string;
  identidad: Usuario;
  statusResponse: string;
  usuarios: any[];
  citas: Cita[];
  selectedUser: any = "Seleccione Usuario";
  flag: boolean = false
  relList: any;


  constructor(private _usuariosService: UsuarioService, private _profesionalesService: ProfesionalesService, private _citasService: CitasService, public dialog: MatDialog) {

    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();
    this._usuariosService.getUserList(this.token).subscribe(
      Response => {
        this.statusResponse = Response.status;

        if (this.statusResponse === 'success') {
          this.usuarios = Response.data;
     
        } 
      },
      error => {
  
        console.log(error);
      });
  }

  onDelete() {
    this.flag=false;
  }

  changeState(idcita: number, idatencion: number)
  {
    this._citasService.updateStateAtencion(this.token, idcita,idatencion).subscribe(

      Response => {
  
        this.statusResponse = Response.status;
        if(this.statusResponse === 'success')
        {
          this.refreshTable(idatencion);
        }
      } 
    )
  }

  refreshTable(id: number)
  {
    for (let i = 0; i < this.dataSource.data.length; i++) {
      if (this.dataSource.data[i].id === id){
        this.dataSource.data.splice(i, 1);
        this.dataSource._updateChangeSubscription();
      }
    }
  }

  getRel() {
    this._usuariosService.getRelList(this.token, this.selectedUser.id).subscribe(
      Response => {
        if(Response.status === 'success'){
          this.flag=true;
          this.relList = Response.data;
          this.dataSource=new MatTableDataSource(Response.data);
          this.dataSource.sort = this.sort;
        } else {
          Swal.fire('Error', 'Hubo un problema al obtener el listado de relaciones', 'error');
        }
      } 
    );
  }

  borrarRelacion(element:any)
  {
    Swal.fire(
      {
        title: '¿Estás seguro?',
        type: 'warning',
        text: 'La relación será eliminada del sistema',
        showConfirmButton: true,
        confirmButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        showCancelButton: true,
      }
    ).then ((result) => {
    if (result.value) {
      this._usuariosService.eliminarRelacion(this.token, element).subscribe(
        Response => {
          if (Response.status === 'success') {
        
            Swal.fire('Éxito', 'La relación ha sido borrada de forma exitosa', 'success');

            for (let i = 0; i < this.dataSource.data.length; i++) {
              if (this.dataSource.data[i].id === Response.relacion.id) {
                   this.dataSource.data.splice(i, 1);
                    this.dataSource._updateChangeSubscription();
              }
            }
          } else {
            Swal.fire('Error', Response.message, 'error');
          }
        },
        error => {
          console.log(error);
          Swal.fire('Error', 'Ups! Algo salio mal!', 'error');
        });
    }
 });
  }

  agregarRelacion(){
    if(this.selectedUser.id)
{
  let dialogRef = this.dialog.open(ModalAddRelComponent, {
    disableClose: false,
    minWidth: "25%",
    maxWidth: "25%",
    data: {
      element:this.selectedUser
          }
  });
}
else
{
  Swal.fire("Seleccione un usuario");
}
}

   

}
