import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';

// Servicios
import { UsuarioService } from '../../../services/usuario/usuario.service';
import { CitasService } from 'src/app/services/citas/citas.service';
import { APIURL } from 'src/app/services/apiUrl';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Paciente } from 'src/app/models/paciente';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import { ObrasSocialesService } from 'src/app/services/obrassociales/obrassociales.service';
import { ProfesionalesService } from 'src/app/services/profesionales/profesionales.service';

@Component({
  selector: 'app-modal-addRel',
  templateUrl: './modalAddRel.component.html',
  styleUrls: ['./modalAddRel.component.css']
})
export class ModalAddRelComponent implements OnInit {

  token: string;
  statusResponse: string;
  profesionales: any = [];
  profesionalSelected: any;
  datos: any;
  constructor(private _formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any,
              private _userService: UsuarioService, private renderer: Renderer2, public dialog: MatDialog,
              public dialogRef: MatDialogRef<ModalAddRelComponent>, private _profesionalesService: ProfesionalesService, public router: Router, private _citasService: CitasService, private _usuariosService: UsuarioService) {
      console.log(this.data);
  this.datos= this.data.element;
  this.token = this._userService.getToken();
  this._profesionalesService.getProfesionales(this.token).subscribe(
    Response => {
      this.statusResponse = Response.status;

      if (this.statusResponse === 'success') {
        this.profesionales = Response.data;
   
      } 
    },
    error => {

      console.log(error);
    });
  }

  ngOnInit() {

  }

  close() {
    this.dialogRef.close();
  }

  addRel() {

    // console.log(this.datos.id);
    // console.log(this.profesionales.id);
    this._userService.agregarRelacion(this.token, this.datos.id, this.profesionalSelected.id).subscribe(
      Response => {
        if (Response.status === 'success') 
        {
            Swal.fire('Relacion cargada', 'La relacion fue cargada de forma exitosa', 'success');
            this.dialogRef.close(Response.data);
            window.location.reload();
        }
        // IMPROVE
        else if(Response.status == 'error')
        {
          console.log(Response.message);
        }
    },
      error => 
      {
        Swal.fire('Error', error.statusText, 'error');
        this.dialog.closeAll();
      }
    )
    
  }

}
