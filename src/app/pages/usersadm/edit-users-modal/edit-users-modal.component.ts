import { Component, Inject, OnInit } from '@angular/core';
import { Paciente } from 'src/app/models/paciente';
import { UsuarioService } from 'src/app/services/services.index';
import { ObrasSocialesService } from 'src/app/services/obrassociales/obrassociales.service';
import { Usuario } from 'src/app/models/usuario';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-edit-users-modal',
  templateUrl: './edit-users-modal.component.html',
  styleUrls: ['./edit-users-modal.component.css']
})
export class EditUsersModalComponent implements OnInit {

  paciente: Paciente;
  sexo: any;
  tipodoc: any;
  token: string;  
  identidad: Usuario;
  statusResponse: string;
  obrassociales: any;

  constructor(private _usuariosService: UsuarioService, private _obrassocialesService: ObrasSocialesService, @Inject(MAT_DIALOG_DATA) public data: any,private _pacientesService: PacientesService) {
    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();
   
    this.sexo= ['Mujer', 'Hombre']
    this.tipodoc=['DNI', 'LC', 'LE', 'Otro'];
    this.paciente=data.element;

    if(this.paciente.sexo=="0" || this.paciente.sexo=="1")
     { this.paciente.sexo="";
    }
    // if(this.paciente.tipodoc=="0"){
    //   this.paciente.tipodoc=="";
      
    // }

    this._obrassocialesService.getObrasSociales(this.token).subscribe(
      Response => {
        this.statusResponse = Response.status;

        if (this.statusResponse === 'success') {
        this.obrassociales= Response.data;
          } 
      },
      error => {
  
        console.log(error);
      });


   }

  ngOnInit() {
  }

  OnSubmit()
  {
  
   
    this._pacientesService.updatePaciente(this.token, this.paciente).subscribe(
      Response => {
       
        if (this.statusResponse === 'success') {
      
            Swal.fire('Paciente actualizado', 'El paciente fue actualizado de forma exitosa', 'success');
         
        }
        else if(this.statusResponse == 'error')
        {
          console.log(Response.message);
        }
    },
      error => {
        Swal.fire('Error', error.statusText, 'error');
      })
  }



}
