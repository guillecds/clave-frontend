import { Component,  ViewChild, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, MatSortable } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UsuarioService } from 'src/app/services/services.index';
import { Usuario } from 'src/app/models/usuario';
import { ModalHistoryComponent } from '../lista/modalhistory/modalhistory.component';
import { EditUsersModalComponent } from '../usersadm/edit-users-modal/edit-users-modal.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { AddUsersModalComponent } from './add-users-modal/add-users-modal.component';


@Component({
  selector: 'app-usersadm',
  templateUrl: './usersadm.component.html',
  styleUrls: ['./usersadm.component.css']
})
export class UsersAdmComponent implements OnInit {
  
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>([]);
  displayedColumns: string[] = ['username', 'nivel', 'opciones'];

  token: string;  
  identidad: Usuario;
  statusResponse: string;
  pacientes: any;
  itemPerPage = 20;
  constructor(private _usuariosService: UsuarioService, public dialog: MatDialog, ) { 
    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();

    this._usuariosService.getUserList(this.token).subscribe(
      Response => {
        this.statusResponse = Response.status;
        if (this.statusResponse === 'success') {
          this.dataSource = new MatTableDataSource(Response.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          } 
      },
      error => {
        console.log(error);
      });
  }
  
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.filterPredicate = function(data, filter: string): boolean {
      return   data.username.toLowerCase().includes(filter)        
  };  
  }

  restablecer(element: any)
  {
    Swal.fire(
      {
        title: '¿Estás seguro?',
        type: 'warning',
        text: 'La contraseña será restablecida',
        showConfirmButton: true,
        confirmButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        showCancelButton: true,
      }).then ((result) => {
        if (result.value) {
            this._usuariosService.recoverPass(element).subscribe(
              Response => {
               console.log(Response);
               if (Response.status === 'success') {
                Swal.fire('Exito', 'El enlace para restablecer su contraseña fue enviado a su dirección de correo electrónico', 'success');
               } else {
                 Swal.fire('Cuidado!', 'Hubo un error al enviar el correo de recuperación', 'warning');
                 console.log(Response.message);
                 }
               },
              error => {
                 console.log('error: ', error);
             });
        }
     });


  }

  eliminarUsuario(element:any)
  {
    Swal.fire(
      {
        title: '¿Estás seguro?',
        type: 'warning',
        text: 'El usuario será eliminado del sistema',
        showConfirmButton: true,
        confirmButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        showCancelButton: true,
      }
    ).then ((result) => {
    if (result.value) {
      this._usuariosService.eliminarUsuario(this.token, element).subscribe(
        Response => {
          if (Response.status === 'success') {
        
            Swal.fire('Éxito', 'El usuario ha sido borrado de forma exitosa', 'success');

            for (let i = 0; i < this.dataSource.data.length; i++) {
              if (this.dataSource.data[i].id === Response.user.id) {
                   this.dataSource.data.splice(i, 1);
                    this.dataSource._updateChangeSubscription();
              }
            }
          } else {
            Swal.fire('Error', Response.message, 'error');
          }
        },
        error => {
          console.log(error);
          Swal.fire('Error', 'Ups! Algo salio mal!', 'error');
        });
    }
 });
  }

  addUsuario(): void
  { 
    this.dialog.open(AddUsersModalComponent, {
      disableClose: false,
      minWidth: "45%",
      minHeight: "30%",
      data: {
     
            }
    });
  }


  ngOnInit() {

  }

}
