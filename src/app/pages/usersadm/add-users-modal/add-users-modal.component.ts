import { Component, Inject, OnInit } from '@angular/core';

import { Paciente } from 'src/app/models/paciente';
import { UsuarioService } from 'src/app/services/services.index';
import { ObrasSocialesService } from 'src/app/services/obrassociales/obrassociales.service';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';

import { Usuario } from 'src/app/models/usuario';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { ProfesionalesService } from 'src/app/services/profesionales/profesionales.service';
@Component({
  selector: 'app-add-users-modal',
  templateUrl: './add-users-modal.component.html',
  styleUrls: ['./add-users-modal.component.css']
})
export class AddUsersModalComponent implements OnInit {

  
  usuario: Usuario;
  nivel: any;
  token: string;  
  identidad: Usuario;
  statusResponse: string;
  profesionales: any;

  constructor(private _usuariosService: UsuarioService, private _obrassocialesService: ObrasSocialesService, private _profesionalesService: ProfesionalesService, private _pacientesService: PacientesService) { 
    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();
    this.usuario = new Usuario (null, null, null, null, null, null, null, null, null);
    this.nivel = [
      { "short": "AD", "lvl": "Admin" },
      { "short": "AS", "lvl": "Asistente" },
      { "short": "GS", "lvl": "Supervisor de grupo" }
  ];
  this._profesionalesService.getProfesionales(this.token).subscribe(
    Response => {
      this.statusResponse = Response.status;

      if (this.statusResponse === 'success') {
        this.profesionales = Response.data;
 
      } 
    },
    error => {
      console.log(error);
    });
  
  }

  ngOnInit() {
  }


  OnSubmit()
  {
   
    this._usuariosService.signUp(this.usuario, this.token).subscribe(
      Response => {
       
        if (Response.status=== 'success') {
      
            Swal.fire('Usuario creado', 'El usuario fue creado de forma exitosa', 'success');
            window.location.reload();
        }
        else if(this.statusResponse == 'error')
        {
          console.log(Response.message);
        }
    },
      error => {
        Swal.fire('Error', error.statusText, 'error');
      })
  //Falta actualizar el datasource!  IMPROVE

  }

}
