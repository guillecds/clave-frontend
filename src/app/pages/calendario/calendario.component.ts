import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

//Models
import { Usuario } from 'src/app/models/usuario';
import { Profesional } from 'src/app/models/profesional';
import { Cita } from 'src/app/models/cita';

//Services 
import { ProfesionalesService } from 'src/app/services/profesionales/profesionales.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { CitasService } from 'src/app/services/citas/citas.service';
import { DatePipe } from '@angular/common'
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ThousandsPipe } from 'src/app/pipes/thousands.pipe';
import { ModalDetailsComponent } from '../lista/modaldetails/modaldetails.component';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ModalAddCitaComponent } from '../lista/modalAddCita/modalAddCita.component';
import { MatDatepickerInputEvent } from '@angular/material';

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.css']
})
export class CalendarioComponent implements OnInit {

  sort:any;
  @ViewChild(MatSort, {static: false}) set content(content: ElementRef)
  {
    this.sort=content;
    if(this.sort)
    {
      this.dataSource.sort=this.sort;
    }
  }
  displayedColumns: string[] = ['Hora', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sobreturno'];
  dataSource= new MatTableDataSource<any>([]);

  selectedProfesional: any = "Seleccione Profesional";
  profesionales: any[];
  citas: any[];
  token: string;
  identidad: Usuario;
  statusResponse: string;
  flag: boolean = false;
  flag2: boolean = false;
  selectedidcita: any;
  comienzo: any;
  finde: any;
  horarios: any;
  minutos: any;
  selectedCita: any;
  startDate: any;
  week: any[];
  
  colorcons: any[] = ['#fdeae3', '#d9fbf9', '#fffef2'];
  consultorios: any;
  selectedCons: any = 0;


  constructor(public datepipe: DatePipe, public dialog: MatDialog, private _usuariosService: UsuarioService, private _profesionalesService: ProfesionalesService, private _citasService: CitasService) {

    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();
    if (this.identidad.rol == 'AD') {
      this._profesionalesService.getProfesionales(this.token).subscribe(
        Response => {
          this.statusResponse = Response.status;
  
          if (this.statusResponse === 'success') {
            this.profesionales = Response.data;
     
          } 
        },
        error => {
          console.log(error);
        });
    } else if (this.identidad.rol == 'GS') {
      this._profesionalesService.getProfesionalId(this.token, this.identidad.idprof).subscribe(
        Response => {
          this.statusResponse = Response.status;
          if (this.statusResponse === 'success') {
            this.selectedProfesional = Response.data;
            this.getCitas();
          } 
        },
        error => {
          console.log(error);
        });
    } else if (this.identidad.rol == 'AS') {
      this._usuariosService.getRelList(this.token, this.identidad.sub).subscribe(
        Response => {
          this.statusResponse = Response.status;
          if (this.statusResponse === 'success') {
            this.profesionales = Response.data;
            this.profesionales.forEach(element => {
              element.id = element.idprof;
            })
          } 
        },
        error => {
          console.log(error);
        });
    }
       
  }

  semanaAnt() {
    this.flag=false;
    this.flag2=true;
    this._citasService.getCitaPorProfesionalXSemanaAnteriorCalendar(this.token, this.selectedProfesional.id, this.comienzo).subscribe(
      Response => {
        this.flag=true;
        this.flag2=false;
        this.citas = Response.data;
        this.comienzo=Response.comienzosemana;
        this.week=Response.semana;
        this.finde=Response.finsemana;
        this.calendarBuild();
      },
      error => {
        this.flag=false;
        console.log(error);
      })
  }
    
  semanaSig() {
    this.flag=false;
    this.flag2=true;
    console.log(this.finde);
    this._citasService.getCitaPorProfesionalXSemanaSiguienteCalendar(this.token, this.selectedProfesional.id, this.finde).subscribe(
      Response => {
        this.flag=true;
        this.flag2=false;
        this.citas = Response.data;
        this.comienzo=Response.comienzosemana;
        this.week=Response.semana;
        this.finde=Response.finsemana;
        this.calendarBuild();
      },
      error => {
        this.flag=false;
        console.log(error);
      })
  }

  calendarCitas(type: string, event: MatDatepickerInputEvent<Date>){
    var d = new Date(event.value) ;
    var firstOfWeek = new Date(d.setDate(d.getDate() - d.getDay()));
    var date =this.datepipe.transform(firstOfWeek, 'dd-MM');
    this.flag=false;
    this.flag2=true;
    this._citasService.getCitaPorProfesionalXSemanaSiguienteCalendar(this.token, this.selectedProfesional.id, date).subscribe(
      Response => {
        this.flag=true;
        this.flag2=false;
        this.citas = Response.data;
        this.comienzo=Response.comienzosemana;
        this.week=Response.semana;
        this.finde=Response.finsemana;
        
        this.calendarBuild();
      },
      error => {
        this.flag=false;
        console.log(error);
      })
  }
   
  getCitas() {
    this.flag=false;
    this.flag2=true;
    this.getHoraProf();
    this._citasService.getCitaPorProfesionalCalendar(this.token, this.selectedProfesional.id).subscribe(
      Response => {
        this.statusResponse = Response.status;
  
        if (this.statusResponse === 'success') {
          this.flag=true;
          this.flag2=false;
          this.citas = Response.data;
          this.comienzo=Response.comienzosemana;
          this.week=Response.semana;
          this.finde=Response.finsemana;
          this.calendarBuild();
        } 
      },
      error => {
        this.flag=false;
        console.log(error);
      });
  }

  getHoraProf(){
    this._profesionalesService.getHoraProf(this.token, this.selectedProfesional.id).subscribe(
      Response => {
        this.statusResponse = Response.status;
        if (this.statusResponse === 'success') {
          this.minutos = Response.minutos[0].citaminutos;
          this.horarios = Response.horarios;
          this.consultorios = Response.consultorios;
        } 
      },
      error => {
        this.flag=false;
        console.log(error);
      });
  }

  calendarBuild(){
    this.filterCitas();
    let horariosBlank = [];
    var startdate = new Date(460800000 + 10800000); //8:00
    var enddate = new Date(514800000 + 10800000); //23:00

    var start = 460800000;
    var end = 514800000;

    var min = this.minutos * 60000;

    while (start <= end) {
        var campoArray = startdate.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});

        var campoHora = startdate.toLocaleTimeString([], {hour: '2-digit'});//
        var campoMinuto = startdate.toLocaleTimeString([], {minute:'2-digit'});//
        var campoNumerico: number = (parseInt(campoHora) *3600000) +  (parseInt(campoMinuto) *60000);
        
        let Lun = {
          id: "",
          idCons: "",
          paciente: ".",
          estado: ".",
          horalarga: 0,
          color: 'white',
          active: false
        };

        let Mar = {
          id: "",
          idCons: "",
          paciente: ".",
          estado: ".",
          horalarga: 0,
          color: 'white',
          active: false
        };

        let Mie = {
          id: "",
          idCons: "",
          paciente: ".",
          estado: ".",
          horalarga: 0,
          color: 'white',
          active: false
        };

        let Jue = {
          id: "",
          idCons: "",
          paciente: ".",
          estado: ".",
          horalarga: 0,
          color: 'white',
          active: false
        };

        let Vie = {
          id: "",
          idCons: "",
          paciente: ".",
          estado: ".",
          horalarga: 0,
          color: 'white',
          active: false
        };

        let Sob = {
          id: "",
          idCons: "",
          paciente: ".",
          estado: ".",
          horalarga: 0,
          color: 'white',
          active: false
        };

        this.horarios.forEach(element => {

          var hmi = element.horaini + ':' + element.minini;
          var a = hmi.split(':'); 
          var ini = ((+parseInt(a[0])) * 60 * 60 + (+parseInt(a[1])) * 60 )*1000;

          var hmf = element.horafin + ':' + element.minfin;
          var b = hmf.split(':'); 
          var fin = ((+parseInt(b[0])) * 60 * 60 + (+parseInt(b[1])) * 60 )*1000;

          switch(element.dia) {
            case "Lun": { 
              if(((campoNumerico) >= (ini)) && ((campoNumerico) <= (fin) )){
                let i = 0, e=0;
                  while(this.consultorios[e].id != element.idcons && e<3){
                    i++;
                    e++;
                  }
                this.colorcons[i];
                Lun = {
                  id: this.consultorios[i].nombre,
                  idCons: this.consultorios[i].id,
                  paciente: ".",
                  estado: ".",
                  horalarga: campoNumerico,
                  color: this.colorcons[i],
                  active: true
                };
              }
              break; 
            } 
            case "Mar": { 
              if(((campoNumerico) >= (ini)) && ((campoNumerico) <= (fin) )){

                let i = 0, e=0;
                  while(this.consultorios[e].id != element.idcons && e<3){
                    i++;
                    e++;
                  }
                this.colorcons[i];

                Mar = {
                  id: this.consultorios[i].nombre,
                  idCons: this.consultorios[i].id,
                  paciente: ".",
                  estado: ".",
                  horalarga: campoNumerico,
                  color: this.colorcons[i],
                  active: true
                };
              } 
              break; 
            }
            case "Mie": { 
              if(((campoNumerico) >= (ini)) && ((campoNumerico) <= (fin) )){

                let i = 0, e=0;
                  while(this.consultorios[e].id != element.idcons && e<3){
                    i++;
                    e++;
                  }
                this.colorcons[i];

                Mie = {
                  id: this.consultorios[i].nombre,
                  idCons: this.consultorios[i].id,
                  paciente: ".",
                  estado: ".",
                  horalarga: campoNumerico,
                  color: this.colorcons[i],
                  active: true
                };
              } 
              break; 
            }
            case "Jue": { 
              if(((campoNumerico) >= (ini)) && ((campoNumerico) <= (fin) )){

                let i = 0, e=0;
                  while(this.consultorios[e].id != element.idcons && e<3){
                    i++;
                    e++;
                  }
                this.colorcons[i];

                Jue = {
                  id: this.consultorios[i].nombre,
                  idCons: this.consultorios[i].id,
                  paciente: ".",
                  estado: ".",
                  horalarga: campoNumerico,
                  color: this.colorcons[i],
                  active: true
                };
              } 
              break; 
            }
            case "Vie": { 
              if(((campoNumerico) >= (ini)) && ((campoNumerico) <= (fin) )){

                let i = 0, e=0;
                  while(this.consultorios[e].id != element.idcons && e<3){
                    i++;
                    e++;
                  }
                this.colorcons[i];

                Vie = {
                  id: this.consultorios[i].nombre,
                  idCons: this.consultorios[i].id,
                  paciente: ".",
                  estado: ".",
                  horalarga: campoNumerico,
                  color: this.colorcons[i],
                  active: true
                };
              } 
              break; 
            }
            case "Sab": { 
              if(((campoNumerico) >= (ini)) && ((campoNumerico) <= (fin) )){

                let i = 0, e=0;
                  while(this.consultorios[e].id != element.idcons && e<3){
                    i++;
                    e++;
                  }
                this.colorcons[i];

                Sob = {
                  id: this.consultorios[i].nombre,
                  idCons: this.consultorios[i].id,
                  paciente: ".",
                  estado: ".",
                  horalarga: campoNumerico,
                  color: "#f4f4f4",
                  active: true
                };
              } 
              break; 
            }
          }
        });

        

        // var fila = { horalarga: campoNumerico, horaText: campoArray, estado: "DISPONIBLE" };
        this.citas.forEach(element => {
          if(element.hora == campoArray)
          {
            console.log(element);
            switch(element.dia) { 
              case "Lun": { 
                Lun = {
                  id: element.id,
                  idCons: '',
                  paciente: element.paciente,
                  estado: element.estado,
                  horalarga: campoNumerico,
                  color: '',
                  active: true
                };
                break; 
              } 
              case "Mar": { 
                Mar = {
                  id: element.id,
                  idCons: '',
                  paciente: element.paciente,
                  estado: element.estado,
                  horalarga: campoNumerico,
                  color: '',
                  active: true
                }; 
                break; 
              }
              case "Mie": { 
                Mie = {
                  id: element.id,
                  idCons: '',
                  paciente: element.paciente,
                  estado: element.estado,
                  horalarga: campoNumerico,
                  color: '',
                  active: true
                }; 
                break; 
              }
              case "Jue": { 
                Jue = {
                  id: element.id,
                  idCons: '',
                  paciente: element.paciente,
                  estado: element.estado,
                  horalarga: campoNumerico,
                  color: '',
                  active: true
                }; 
                break; 
              }
              case "Vie": { 
                Vie = {
                  id: element.id,
                  idCons: '',
                  paciente: element.paciente,
                  estado: element.estado,
                  horalarga: campoNumerico,
                  color: '',
                  active: true
                }; 
                break; 
              }
              case "Sab": { 
                Sob = {
                  id: element.id,
                  idCons: '',
                  paciente: element.paciente,
                  estado: element.estado,
                  horalarga: campoNumerico,
                  color: '',
                  active: true
                }; 
                break; 
              }
           }
          }
        });
        var fila = { Hora: campoArray, Lunes: Lun, Martes: Mar, Miercoles: Mie, Jueves: Jue, Viernes: Vie, Sobreturno: Sob};
        horariosBlank.push(fila);
        start += min;
        startdate = new Date(start + 10800000);
    }
    this.dataSource = new MatTableDataSource(horariosBlank);
  }

  onDelete() {
    this.flag=false;
  }

  filterCitas() {
    let array = [];
     this.citas.forEach(element => {
       if(element.estado != "CANCELADA"){
         let row = {
            hora: element.fecha.slice(element.fecha.length - 5),
            dia: element.dia,
            id: element.id,
            paciente: element.paciente,
            estado: element.estado
         };
         array.push(row);
       }
     });
     this.citas = array;
  }

  modalCita(id, idCons, horalarga, dia, estado){
    console.log('Abri una cita');
    console.log('estad',estado);
    if(estado != "."){
      console.log(id);
      this._citasService.getCitaPorId(this.token, id).subscribe(
        Response => {
          this.statusResponse = Response.status;
          if (this.statusResponse === 'success') {
            this.selectedCita = Response.cita;
            let dialog = this.dialog.open(ModalDetailsComponent, {
              disableClose: false,
              maxHeight: "45em",
              minWidth: "400px",
              data: {
                    element:this.selectedCita
                    }
            });
            dialog.afterClosed().subscribe(result => {
                this.getCitas()
            });
          } 
        },
        error => {
          // this.flag=false;
          console.log(error);
        });
    }
    else {
      console.log('Debe cargar una cita nueva aquí');
      console.log('a las: ', horalarga);

      let dialog = this.dialog.open(ModalAddCitaComponent, {
        disableClose: false,
        maxHeight: "45em",
        minWidth: "400px",
        //Cargar id ubicación. Script para traer OS. Armar cita con paciente añadido. Ver funcion de cargar cita.
        data: { 
          comienzo: this.comienzo,
          hora: horalarga,
          profesional: this.selectedProfesional,
          diaSemana: dia,
          consultorio: id,
          idCons: idCons
        }
      });
      dialog.afterClosed().subscribe(result => {
        this.getCitas()
      });
    }   
  } 

  ngOnInit() { }
  
}
