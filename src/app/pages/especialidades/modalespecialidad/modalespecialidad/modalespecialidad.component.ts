
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';

//Services
import { UsuarioService } from 'src/app/services/services.index';
import { EspecialidadesService } from 'src/app/services/especialidades/especialidades.service';

//Models
import { Especialidad } from 'src/app/models/especialidad';
import { Usuario } from 'src/app/models/usuario';

@Component({
  selector: 'app-modalespecialidad',
  templateUrl: './modalespecialidad.component.html',
  styleUrls: ['./modalespecialidad.component.css']
})
export class ModalespecialidadComponent implements OnInit {
    token: string;
    identidad: Usuario;
    flag: boolean = true;
    especialidad: Especialidad;
    constructor(@Inject(MAT_DIALOG_DATA) public data: any, private _especialidadesService: EspecialidadesService, private _usuariosService: UsuarioService, private dialog: MatDialog, public dialogRef: MatDialogRef < ModalespecialidadComponent > ) {
        this.token = _usuariosService.getToken();
        this.identidad = _usuariosService.getIdentidad();
        this.especialidad = new Especialidad(null, null);
        if (data.element) {
            this.flag = false;
            this.especialidad = data.element;
        }
    }

    ngOnInit() {}

    OnSubmit() {
        this._especialidadesService.guardarEspecialidad(this.token, this.especialidad).subscribe(
            Response => {
                if (Response.status === 'success') {
                    Swal.fire('Especialidad cargada', Response.message, 'success').then(() => {
                        this.dialogRef.close(Response.data);
                    });
                }
            },
            error => {
                Swal.fire('Error', error.error.message, 'error').then(() => {
                    this.dialog.closeAll();
                });
            })

    }

    actualizar() {
        this._especialidadesService.actualizarEspecialidad(this.token, this.especialidad).subscribe(
            Response => {
                if (Response.status === 'success') {
                    Swal.fire('Especialidad cargada', Response.message, 'success').then(() => {
                        this.dialog.closeAll();
                    });
                }
            },
            error => {
                Swal.fire('Error', error.statusText, 'error').then(() => {
                    this.dialog.closeAll();
                });
            })
    }

}