import { Component,ViewChild, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, MatSortable } from '@angular/material/sort';
import { UsuarioService } from 'src/app/services/services.index';
import { EspecialidadesService } from 'src/app/services/especialidades/especialidades.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Usuario } from 'src/app/models/usuario';
import Swal from 'sweetalert2';
import { ModalespecialidadComponent } from './modalespecialidad/modalespecialidad/modalespecialidad.component';
import { Especialidad } from 'src/app/models/especialidad';

@Component({
  selector: 'app-especialidades',
  templateUrl: './especialidades.component.html',
  styleUrls: ['./especialidades.component.css']
})
export class EspecialidadesComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>([]);
  displayedColumns: string[] = ['especialidad', 'editar', 'borrar'];

  token: string;  
  identidad: Usuario;
  especialidades: Especialidad[];

  constructor(private _usuariosService: UsuarioService, private _especialidadesService: EspecialidadesService, public dialog: MatDialog) {
    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();

    this._especialidadesService.getEspecialidades(this.token).subscribe(
      Response => {
       
        if (Response.status === 'success') {
          this.dataSource = new MatTableDataSource(Response.data);
          this.especialidades = Response.data;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          } 
      },
      error => {
        console.log(error);
      });
   }

  ngOnInit() {}

  applyFilter(filterValue: string) 
  {
      this.dataSource.filter = filterValue.trim().toLowerCase();
      this.dataSource.filterPredicate = function(data, filter: string): boolean 
      {  return data.especialidad.toLowerCase().includes(filter) };  
  }

  openModalEspecialidad(element:any): void 
  {
      let dialogRef = this.dialog.open(ModalespecialidadComponent, {
       disableClose: false,
       minWidth: "30%",
       minHeight: "10%",
       data: {
         element:element
             }
     });
      dialogRef.afterClosed().subscribe(result => {
        {
          if(result)
          {
            window.location.reload();
            // this.especialidades.push(result);
            // this.dataSource = new MatTableDataSource(this.especialidades);
            // this.dataSource.sort = this.sort;
            // this.dataSource._updateChangeSubscription();
            // IMPROVE
          }
        }
        });
}

  borrarEspecialidad(element:any)
  {
    Swal.fire(
      {
        title: '¿Estás seguro?',
        type: 'warning',
        text: 'La especialidad será eliminada del sistema',
        showConfirmButton: true,
        confirmButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        showCancelButton: true,
      }
    ).then ((result) => {
    if (result.value) {
      this._especialidadesService.destroy(this.token, element).subscribe(
        Response => {
          if (Response.status === 'success') {
        
            Swal.fire('Éxito', 'La especialidad ha sido borrada de forma exitosa', 'success');

            for (let i = 0; i < this.dataSource.data.length; i++) {
              if (this.dataSource.data[i].id === Response.especialidad.id) {
                   this.dataSource.data.splice(i, 1);
                    this.dataSource._updateChangeSubscription();
                    // IMPROVE
              }
            }
          } else {
            Swal.fire('Error', Response.message, 'error');
          }
        },
        error => {
          console.log(error);
          Swal.fire('Error', 'Ups! Algo salio mal!', 'error');
        });
    }
 });
  }

}
