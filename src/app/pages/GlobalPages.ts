export const CONDITIONS_LIST = [
    // { value: 'nono', label: 'Nono' },
    { value: 'es-diferente', label: 'Diferente' },
    { value: 'es-igual', label: 'Igual' },
    { value: 'contiene', label: 'Contiene' }
  ];

  export const CONDITIONS_FUNCTIONS = { // search method base on conditions list value
    // 'is-empty': function (value, filterdValue) {
    //   return value === '';
    // },
    'es-igual': function (value, filterdValue) {
      return value === filterdValue;
    },
    'contiene': function (value, filterdValue) {
        // console.log('filterdValue: ', filterdValue);
        // console.log('value: ', value);
        if (typeof value !== 'object') {
            return (value.toLowerCase().search(filterdValue.toLowerCase()) === -1) ? false  : true ;
        }
        // else {
        //     return (value.descripcion.toLowerCase().search(filterdValue.toLowerCase()) === -1) ? false  : true ;
        // }
    },
    'es-diferente': function (value, filterdValue) {
      return value !== filterdValue;
    }
  };
