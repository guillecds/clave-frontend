import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';

// Servicios
import { UsuarioService } from '../../../services/usuario/usuario.service';
import { CitasService } from 'src/app/services/citas/citas.service';

// Componentes
import Swal from 'sweetalert2';
import * as _ from 'lodash';
import { APIURL } from 'src/app/services/apiUrl';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal-estado',
  templateUrl: './modalestado.component.html',
  styleUrls: ['./modalestado.component.css']
})
export class ModalEstadoComponent implements OnInit {

  // Usuario
  token: string;
  selectedEstado: any;
  estados: any = [
    {option: 'PENDIENTE', val: 'PENDIENTE'},
    {option: 'CONFIRMADA', val: 'CONFIRMADA'},
    {option: 'EN SALA', val: 'EN_SALA'},
    {option: 'ATENDIDA', val: 'ATENDIDA'},
    {option: 'AUSENTE', val: 'AUSENTE'},
    {option: 'CANCELADA', val: 'CANCELADA'},
    {option: 'ANULADA', val: 'ANULADA'},
    {option: 'PRIORITARIA', val: 'PRIORITARIA'},
    {option: 'NO DAR TURNO', val: 'NO DAR TURNO'},
    {option: 'SOBRETURNO', val: 'SOBRETURNO'},
  ];

  statusResponse: string;


  constructor(private _formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any,
              private _userService: UsuarioService, private renderer: Renderer2, public dialog: MatDialog,
              public dialogRef: MatDialogRef<ModalEstadoComponent>, public router: Router, private _citasService: CitasService ) {
      
    this.token = this._userService.getToken();

  }


  ngOnInit() {}

  // cerrar dialog
  close() {
    this.dialogRef.close();
  }

  saveCita() {
    console.log(this.data.idCita);
    console.log(this.selectedEstado.val);
     this._citasService.updateStateCita(this.token, this.data.idCita, this.selectedEstado.val).subscribe(
       Response => {
         this.statusResponse = Response.status;
         if(this.statusResponse === 'success'){
           this.dialogRef.close(this.selectedEstado);
         }
       } 
     )
  }

}
