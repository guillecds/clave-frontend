import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';

//Models
import { Usuario } from 'src/app/models/usuario';
import { Profesional } from 'src/app/models/profesional';
import { Cita } from 'src/app/models/cita';

//Services 
import { ProfesionalesService } from 'src/app/services/profesionales/profesionales.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { CitasService } from 'src/app/services/citas/citas.service';

import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalEstadoComponent } from './modalestado/modalestado.component';

export interface PacientesBandeja {
  dia: string;
  fecha: string;
  notas: string;
  anterior: string;
  paciente: string;
  tel: string;
  consultorio: string;
  estado: string;
  opciones: string;
}


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})


export class DashboardComponent implements OnInit {

  sort:any;
  @ViewChild(MatSort, {static: false}) set content(content: ElementRef)
  {
    this.sort=content;
    if(this.sort)
    {
      this.dataSource.sort=this.sort;
    }
  }
  // displayedColumns: string[] = [ 'dia', 'fecha', 'notas', 'anterior', 'paciente', 'tel', 'consultorio', 'estado', 'opciones'];
  displayedColumns: string[] = [ 'dia', 'fecha', 'notas', 'paciente', 'tel', 'consultorio', 'estado', 'opciones'];
  dataSource= new MatTableDataSource<any>([]);
  token: string;
  identidad: Usuario;
  statusResponse: string;
  comienzo: any;
  finde: any;
  state: string = 'pendiente';
  profesionales: any[];
  citas: Cita[];

  selectedProfesional: any = "Seleccione Profesional";
  flag: boolean = false;
  flag2: boolean = false;
  lengthflag: boolean = true;
  selectedidcita: any;
  
  constructor(public dialog: MatDialog, private _usuariosService: UsuarioService, private _profesionalesService: ProfesionalesService, private _citasService: CitasService) {

    this.token = _usuariosService.getToken();
    this.identidad = _usuariosService.getIdentidad();
    
    if (this.identidad.rol == 'AD') {
      this._profesionalesService.getProfesionales(this.token).subscribe(
        Response => {
          this.statusResponse = Response.status;
  
          if (this.statusResponse === 'success') {
            this.profesionales = Response.data;
     
          } 
        },
        error => {
          console.log(error);
        });
    } else if (this.identidad.rol == 'GS') {
      this._profesionalesService.getProfesionalId(this.token, this.identidad.idprof).subscribe(
        Response => {
          this.statusResponse = Response.status;
          if (this.statusResponse === 'success') {
            this.selectedProfesional = Response.data;
            this.getCitas();
          } 
        },
        error => {
          console.log(error);
        });
    } else if (this.identidad.rol == 'AS') {
      this._usuariosService.getRelList(this.token, this.identidad.sub).subscribe(
        Response => {
          this.statusResponse = Response.status;
          if (this.statusResponse === 'success') {
            this.profesionales = Response.data;
            this.profesionales.forEach(element => {
              element.id = element.idprof;
            })
          } 
        },
        error => {
          console.log(error);
        });
    }  
  }

  onDelete() {
    this.flag=false;
  }

  changeState(id: number, estado: string){
    this.selectedidcita = id;
    this.openDialog();
  }

  refreshTable(id: number, estado: string)
  {
    for (let i = 0; i < this.dataSource.data.length; i++) {
      if (this.dataSource.data[i].id === id){
        this.dataSource.data[i].estado = estado;
        this.dataSource._updateChangeSubscription();
      }
    }
  }

  semanaAnt() {
    this.flag=false;
    this.flag2=true;
    this.lengthflag = true;
    this._citasService.getCitaPorProfesionalXSemanaAnterior(this.token, this.selectedProfesional.id, this.comienzo).subscribe(

      Response => {
        this.statusResponse = Response.status;

        if (this.statusResponse === 'success') {
          this.flag=true;
          this.flag2=false;
          this.citas = Response.data;
          this.dataSource=new MatTableDataSource(Response.data);
          this.dataSource.sort = this.sort;
          this.comienzo=Response.comienzosemana;
          this.finde=Response.finsemana;
          if(this.citas.length > 0)
            this.lengthflag = false;
        } 
      },
      error => {
        this.flag=false;
        console.log(error);
      })
  }
    
  semanaSig() {
    this.flag=false;
    this.flag2=true;
    this.lengthflag = true;
    this._citasService.getCitaPorProfesionalXSemanaSiguiente(this.token, this.selectedProfesional.id, this.finde).subscribe(
      
      Response => {
        this.statusResponse = Response.status;
  
        if (this.statusResponse === 'success') {
          this.flag=true;
          this.flag2=false;
          this.citas = Response.data;
          this.dataSource=new MatTableDataSource(Response.data);
          this.dataSource.sort = this.sort;
          this.comienzo=Response.comienzosemana;
          this.finde=Response.finsemana;
          if(this.citas.length > 0)
            this.lengthflag = false;
        } 
      },
      error => {
        this.flag=false;
        console.log(error);
      })
  }
  
  getCitas() {
    this.flag=false;
    this.flag2=true;
    this.lengthflag=true;
    this._citasService.getCitaPorProfesional(this.token, this.selectedProfesional.id).subscribe(
      Response => {
        this.statusResponse = Response.status;

        if (this.statusResponse === 'success') {
          this.flag=true;
          this.flag2=false;
          this.citas = Response.data;
          this.dataSource=new MatTableDataSource(Response.data);
          this.dataSource.sort = this.sort;
          this.comienzo=Response.comienzosemana;
          this.finde=Response.finsemana;
          if(this.citas.length > 0)
            this.lengthflag = false;
        } 
      },
      error => {
        this.flag=false;
        console.log(error);
      });

  }


  openDialog(): void {

    let dialog = this.dialog.open(ModalEstadoComponent, {
      disableClose: false,
      minHeight: "500px",
      data: {
              idCita: this.selectedidcita
            }
    });

    dialog.afterClosed().subscribe(result => {
      if(result)
        this.refreshTable(this.selectedidcita, result.val);
    });
    
  }

  ngOnInit() { }

}
