
import { Component, Inject, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/services.index';
import { Usuario } from 'src/app/models/usuario';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ObrasSocialesService} from 'src/app/services/obrassociales/obrassociales.service';
import { ObraSocial} from 'src/app/models/obrasocial';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-obra-social-modal',
  templateUrl: './obra-social-modal.component.html',
  styleUrls: ['./obra-social-modal.component.css']
})

export class ObraSocialModalComponent implements OnInit {

  token: string;
  identidad: Usuario;
  statusResponse: string;
  flag: boolean = true;
  obrasocial: ObraSocial;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private _obrasSocialesService: ObrasSocialesService, private _usuariosService: UsuarioService, public dialogRef: MatDialogRef < ObraSocialModalComponent > , private dialog: MatDialog) {
      this.token = _usuariosService.getToken();
      this.identidad = _usuariosService.getIdentidad();
      this.obrasocial = new ObraSocial(null, null, null, null);
      if (data.element) {
          this.flag = false;
          this.obrasocial = data.element;
      }
  }

  ngOnInit() {}

  OnSubmit() {
      this._obrasSocialesService.guardarObraSocial(this.token, this.obrasocial).subscribe(
          Response => {
              if (Response.status === 'success') {
                  Swal.fire('Obra social cargada', 'La obra social fue agregada de forma exitosa', 'success').then(() => {
                      this.dialogRef.close(Response.data);
                      // IMPROVE with Response.message in swal.fire
                  });
              }
          },
          error => {
              Swal.fire('Error', error.statusText, 'error').then(() => {
                  this.dialog.closeAll();
              });
          })
  }

  actualizar() {
      this._obrasSocialesService.actualizarObraSocial(this.token, this.obrasocial).subscribe(
          Response => {
              if (Response.status === 'success') {
                  Swal.fire('Obra social actualizada', 'La obra social fue actualizada de forma exitosa', 'success').then(() => {
                      this.dialog.closeAll();
                      // IMPROVE with Response.message in swal.fire

                  });
              }
          },
          error => {
              Swal.fire('Error', error.statusText, 'error').then(() => {
                  this.dialog.closeAll();
              });
          })
  }

}