import { Component,ViewChild, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/services.index';
import Swal from 'sweetalert2';
import { MatSort, MatSortable } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ObrasSocialesService } from 'src/app/services/obrassociales/obrassociales.service';
import { ObraSocialModalComponent } from './obra-social-modal/obra-social-modal.component';
import { ObraSocial } from 'src/app/models/obrasocial';


@Component({
  selector: 'app-obrassociales',
  templateUrl: './obrassociales.component.html',
  styleUrls: ['./obrassociales.component.css']
})
export class ObrasSocialesComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true}) paginator: MatPaginator;
  dataSource = new MatTableDataSource < any > ([]);
  displayedColumns: string[] = ['nombre', 'sigla', 'descripcion', 'acciones'];

  token: string;
  identidad: Usuario;
  obrasSociales: ObraSocial[];

  constructor(private _usuariosService: UsuarioService, private _obrasSocialesService: ObrasSocialesService, public dialog: MatDialog) {

      this.token = _usuariosService.getToken();
      this.identidad = _usuariosService.getIdentidad();

      this._obrasSocialesService.getObrasSociales(this.token).subscribe(
          Response => {
              if (Response.status === 'success') {
                  this.dataSource = new MatTableDataSource(Response.data);
                  this.obrasSociales = Response.data;
                  this.dataSource.paginator = this.paginator;
                  this.dataSource.sort = this.sort;
              }
          },
          error => {
              console.log(error);
          });
  }

  ngOnInit() {}
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.filterPredicate = function(data, filter: string): boolean {
        return data.nombre.toLowerCase().includes(filter)
    };
}

openModalObraSocial(element: any): void {
  let dialogRef = this.dialog.open(ObraSocialModalComponent, {
      disableClose: false,
      minWidth: "30%",
      minHeight: "10%",
      data: {
          element: element
      }
  });

  dialogRef.afterClosed().subscribe(result => {

    if(result)
    {
        window.location.reload();
    //   this.obrasSociales.push(result);
    //   this.dataSource = new MatTableDataSource(this.obrasSociales);
    //   this.dataSource.sort = this.sort;
    //   this.dataSource._updateChangeSubscription();
      //  IMPROVE  
    }
  
  });

}


  deleteObraSocial(element: any) {
      Swal.fire({
          title: '¿Estás seguro?',
          type: 'warning',
          text: 'La obra social será eliminada del sistema',
          showConfirmButton: true,
          confirmButtonColor: '#d33',
          confirmButtonText: 'Confirmar',
          showCancelButton: true,
      }).then((result) => {
          if (result.value) {
              this._obrasSocialesService.destroyObraSocial(this.token, element).subscribe(
                  Response => {
                      if (Response.status === 'success') {

                          Swal.fire('Éxito', 'La obra social ha sido borrada de forma exitosa', 'success');

                          for (let i = 0; i < this.dataSource.data.length; i++) {
                              if (this.dataSource.data[i].id === Response.data.id) {
                                  this.dataSource.data.splice(i, 1);
                                  this.dataSource._updateChangeSubscription();
                                  // IMPROVE
                              }
                          }
                      } else {
                          Swal.fire('Error', Response.message, 'error');
                      }
                  },
                  error => {
                      console.log(error);
                      Swal.fire('Error', 'Ups! Algo salio mal!', 'error');
                  });
          }
      });
  }


}
