import { Component, OnInit, HostListener } from '@angular/core';
// MODELOS
import { Usuario } from '../../models/usuario';

// SERVICIOS
import { UsuarioService } from '../../services/usuario/usuario.service';

// ROUTER
import { Router } from '@angular/router';

import Swal from 'sweetalert2';
import { APIURL } from '../../services/apiUrl';
import { ExcelService } from '../../services/excel/excel.service';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [ExcelService]
})

export class HeaderComponent implements OnInit {
  token: string;
  identidad: any;
  pdfCatalogo: string;
  usuario: any;
  flag: boolean;
  categories: string;
  cantArt:number;
  categ:number = 999;
  catestr:string = '';
  searchTerm:string = '';
  p: number = 1;
  // articulos obtenidos
  articulos: any[] = [];
  filteredArticulos: any[] = [];
  hidecat: boolean=true;
 
  loading:boolean = false;
 
  // Bandera
  dataReady: boolean = false;

  //Posiciones iniciales para scroll menu
  prevScrollpos:any = document.documentElement.scrollTop || document.body.scrollTop;
  currentScrollPos:any = document.documentElement.scrollTop || document.body.scrollTop;

  constructor( private _usuariosServices: UsuarioService, private _router: Router,  private _excelService: ExcelService) {
     
    this.usuario = _usuariosServices.getIdentidad();
    this.flag = false;
    this.token = _usuariosServices.getToken();
  }

    logout() {
      // this.intercom.shutdown();
      localStorage.removeItem('token');
      localStorage.removeItem('identidad');
      this._router.navigate(['/login']);
    }
    
  ngOnInit() {
    
    
  }

  doSomethingOnWindowScroll(){
    this.currentScrollPos = document.documentElement.scrollTop || document.body.scrollTop;

    if (this.prevScrollpos > this.currentScrollPos) {
      document.getElementById("bannercm").style.top = "0";
    } else {
      document.getElementById("bannercm").style.top = "-25.4%";
    }
    this.prevScrollpos = this.currentScrollPos;
  }
  
}
