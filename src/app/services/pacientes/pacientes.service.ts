import { Injectable } from '@angular/core';

// Otros
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APIURL } from '../apiUrl';

// Modelos
import { Paciente } from '../../models/paciente'; 

@Injectable({
  providedIn: 'root'
})

export class PacientesService {
  url: string;
  identity;
  token;

  constructor(public _http: HttpClient) { this.url = APIURL.url; }

  getPacientes(token: string): Observable < any > {
      let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded').set('Authorization', token);
      return this._http.get(this.url + 'getPacientes', { headers: headers });
  }

  getAllPacientes(token: string): Observable < any > {
      let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded').set('Authorization', token);
      return this._http.get(this.url + 'getAllPacientes', {
          headers: headers
      });
  }

  destroy(token: string, id: any): Observable < any > {
      let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded').set('Authorization', token);
      return this._http.delete(this.url + 'destroyPaciente/' + id, { headers: headers
      });
  }

  guardarPaciente(token: string, paciente: Paciente): Observable < any > {
      let params = JSON.stringify(paciente);
      let headers = new HttpHeaders().set('Content-type', 'application/json').set('Authorization', token);
      return this._http.post(this.url + 'storePaciente', params, { headers: headers });
  }

  updatePaciente(token: string, paciente: Paciente): Observable < any > {
      let params = JSON.stringify(paciente);
      let headers = new HttpHeaders().set('Content-type', 'application/json').set('Authorization', token);
      return this._http.put(this.url + 'updatePaciente', params, {headers: headers });
      // IMPROVE (enviar id al update, arreglar en back)   

  }

  pacientexDoc(token: string, doc: any): Observable < any > {
      let json = JSON.stringify(doc);
      let params = 'json=' + json;
      let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded').set('Authorization', token);
      return this._http.post(this.url + 'pacientexdoc', params, { headers: headers });
  }

  estudiosxPaciente(token: string, idPac: any): Observable < any > {
    let json = JSON.stringify(idPac);
    let params = 'json=' + json;
    let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded').set('Authorization', token);
    return this._http.post(this.url + 'estudioxpaciente', params, { headers: headers });
  }

  getHcInfo(token: string, idPac: any): Observable < any > {
    let json = JSON.stringify(idPac);
    let params = 'json=' + json;
    let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded').set('Authorization', token);
    return this._http.post(this.url + 'getHcInfo', params, { headers: headers });
  }
  
}