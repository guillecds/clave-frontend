// Modulos
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';


import {
   SharedService,
   UsuarioService,
   CargaImagenService,
   StatesService
} from './services.index';




@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  providers: [
   SharedService,
   UsuarioService,
   CargaImagenService,
   StatesService
  ],
  declarations: []
})
export class ServiceModule { }
