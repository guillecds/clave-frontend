import { Injectable } from '@angular/core';

// Otros
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APIURL } from '../apiUrl';

// Modelos
import { Especialidad } from 'src/app/models/especialidad';

@Injectable({
  providedIn: 'root'
})
export class EspecialidadesService {

  url: string;
  identity;
  token;

  constructor(public _http: HttpClient) {
      this.url = APIURL.url;
  }

  getEspecialidades(token: string): Observable < any > {

      let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded').set('Authorization', token);
      return this._http.get(this.url + 'getEspecialidades', { headers: headers });
  }

  destroy(token: string, id: any): Observable < any > {
      let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded').set('Authorization', token);
      return this._http.delete(this.url + 'destroyEspecialidad/' + id, { headers: headers });
  }

  guardarEspecialidad(token: string, especialidad: any): Observable < any > {
      let params = JSON.stringify(especialidad);
      let headers = new HttpHeaders().set('Content-type', 'application/json').set('Authorization', token);
      return this._http.post(this.url + 'storeEspecialidad', params, { headers: headers });
  }

  actualizarEspecialidad(token: string, especialidad: Especialidad): Observable < any > {
      let params = JSON.stringify(especialidad);
      let headers = new HttpHeaders().set('Content-type', 'application/json').set('Authorization', token);
      // IMPROVE (enviar id al update, arreglar en back)   
      return this._http.post(this.url + 'updateEspecialidad/', params, { headers: headers});
  }

}