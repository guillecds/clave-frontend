import { Injectable } from '@angular/core';

// Otros
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APIURL } from '../apiUrl';
import { ObraSocial } from 'src/app/models/obrasocial';

// Modelos

@Injectable({
  providedIn: 'root'
})
export class ObrasSocialesService {

  url: string;
  identity;
  token;

  constructor( public _http: HttpClient) {
    this.url = APIURL.url;
    }
  
  getObrasSociales(token: string): Observable < any > {

      let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded').set('Authorization', token);
      return this._http.get(this.url + 'getObrasSociales', { headers: headers });
  }


  destroyObraSocial(token: string, id: any): Observable < any > {

      let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded').set('Authorization', token);
      return this._http.delete(this.url + 'destroyObraSocial/' + id, { headers: headers });
  }

  guardarObraSocial(token: string, obrasocial: ObraSocial): Observable < any > {

      let params = JSON.stringify(obrasocial);
      let headers = new HttpHeaders().set('Content-type', 'application/json').set('Authorization', token);
      return this._http.post(this.url + 'storeObraSocial', params, { headers: headers });
  }

  actualizarObraSocial(token: string, obrasocial: ObraSocial): Observable < any > {

      let params = JSON.stringify(obrasocial);
      let headers = new HttpHeaders().set('Content-type', 'application/json').set('Authorization', token);
      return this._http.post(this.url + 'updateObraSocial', params, { headers: headers });

  }

}