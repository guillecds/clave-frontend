import { Injectable } from '@angular/core';

// Otros
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APIURL } from '../apiUrl';
import { HistoriaClinica } from 'src/app/models/historiaclinica';

// Modelos
// import { Profesional } from '../../models/profesional'; 

@Injectable({
  providedIn: 'root'
})
export class HistoriasService {

  url: string;
  identity;
  token;

  constructor( public _http: HttpClient) {
    this.url = APIURL.url;
  }

getHistorias(token: string, id: number): Observable<any> {
    let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
    .set('Authorization', token);
    return this._http.get(this.url + 'getHistorias/' + id ,  {headers: headers});
  }


storeHistoria(token: string, historia: HistoriaClinica): Observable<any> {
    let json = JSON.stringify(historia);
    let params = 'json=' + json;
    let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
    .set('Authorization', token);
    return this._http.post(this.url + 'storeHistoria', params,  {headers: headers});
  }


}