import { Injectable } from '@angular/core';

// Otros
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APIURL } from '../apiUrl';

// Modelos
import { Antecedentes } from 'src/app/models/antecedentes';

@Injectable({
  providedIn: 'root'
})
export class AntecedentesService {

  url: string;
  identity;
  token;

  constructor( public _http: HttpClient) {
    this.url = APIURL.url;
  }

  
  getAntecedentes(token: string, id:number): Observable<any> 
  {
      let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', token);
      return this._http.get(this.url + 'getAntecedentes/' + id,  {headers: headers});
  }

  
  updateAntecedentes(token: string, antecedentes: Antecedentes): Observable<any> {
  let json = JSON.stringify(antecedentes);
  let params = 'json=' + json;
  let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
  .set('Authorization', token);
  return this._http.post(this.url + 'updateAntecedentes', params,  {headers: headers});
}


}

 