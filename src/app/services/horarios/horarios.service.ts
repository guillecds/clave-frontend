import { Injectable } from '@angular/core';

// Otros
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APIURL } from '../apiUrl';


@Injectable({
  providedIn: 'root'
})
export class HorariosService {

  url: string;
  identity;
  token;

  constructor( public _http: HttpClient) {
    this.url = APIURL.url;
  }

  getHorariosProfesional(token: string, id: number): Observable<any> {
 
    let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
    .set('Authorization', token);

    return this._http.get(this.url + 'getHorariosProfesional/' + id ,  {headers: headers});
    }

    
  destroy(token:string, id: any): Observable<any> {

    let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
    .set('Authorization', token);

    return this._http.delete(this.url + 'destroyHorario/' + id,  {headers: headers});
    }

}