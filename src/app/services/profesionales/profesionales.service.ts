import { Injectable } from '@angular/core';

// Otros
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APIURL } from '../apiUrl';

// Modelos
import { Profesional } from '../../models/profesional'; 

@Injectable({
  providedIn: 'root'
})
export class ProfesionalesService {

  url: string;
  identity;
  token;

  constructor( public _http: HttpClient) {
    this.url = APIURL.url;
  }

  getProfesionalesFull(token: string): Observable<any> {
    let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                      .set('Authorization', token);
    return this._http.get(this.url + 'getProfesionalesFull' , {headers: headers});
  }
  
  getProfesionales(token: string): Observable<any> {
    let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                    .set('Authorization', token);
    return this._http.get(this.url + 'getProfesionales' , {headers: headers});
  }

  getProfesionalId(token: string, id: number): Observable<any> {
    let data = {
      id:id
    };
    let json = JSON.stringify(data);
    let params = 'json=' + json;
    let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                    .set('Authorization', token);
      return this._http.post(this.url + 'getProfesionalId' , params, {headers: headers});
    }

  getHoraProf(token: string, id: number): Observable<any> {
    let data = {
      id:id
    };
    let json = JSON.stringify(data);
    let params = 'json=' + json;
    let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                    .set('Authorization', token);
      return this._http.post(this.url + 'getHoraProf' , params, {headers: headers});
    }

  destroy(token:string, id: any): Observable<any> {
    let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
    .set('Authorization', token);
    return this._http.delete(this.url + 'destroyProfesional/' + id,  {headers: headers});
  }

  guardarProfesional(token: string, profesional: Profesional): Observable<any> {
        let params = JSON.stringify(profesional);
        let headers = new HttpHeaders().set('Content-type', 'application/json').set('Authorization', token);
        return this._http.post(this.url + 'storeProfesional', params,  {headers: headers});
  }

  updateProfesional(token: string, profesional: Profesional): Observable<any> {
    let json = JSON.stringify(profesional);
    let params = 'json=' + json;
    let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
    .set('Authorization', token);
    return this._http.put(this.url + 'updateProfesional', params,  {headers: headers});
  }
      
      // getAtencionProfesional(token:string, id: any): Observable<any> {

      //     let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
      //     .set('Authorization', token);
          
      //     return this._http.get(this.url + 'getAtencionProfesional/' + id,  {headers: headers});
      // }
}

