import { Injectable } from '@angular/core';

// Otros
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APIURL } from '../apiUrl';
import { Consultorio } from 'src/app/models/consultorio';

// Modelos


@Injectable({
  providedIn: 'root'
})
export class ConsultoriosService {

  url: string;
  identity;
  token;

  constructor(public _http: HttpClient) {
    this.url = APIURL.url;
  }

  getConsultoriosProfesional(token: string, id: any): Observable<any> {

    let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', token);

    return this._http.get(this.url + 'getConsultoriosProfesional/' + id, { headers: headers });
  }

  guardarConsultorio(token: string, consultorio: Consultorio): Observable<any> {
    let params = JSON.stringify(consultorio);
    let headers = new HttpHeaders().set('Content-type', 'application/json').set('Authorization', token);
    return this._http.post(this.url + 'storeConsultorio', params, { headers: headers });
  }

  
  destroy(token:string, id: any): Observable<any> {
    let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
    .set('Authorization', token);
    return this._http.delete(this.url + 'destroyConsultorio/' + id,  {headers: headers});
  }


}

