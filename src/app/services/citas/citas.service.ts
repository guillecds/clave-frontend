import { Injectable } from '@angular/core';

// Otros
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APIURL } from '../apiUrl';

// Modelos
import { Cita } from '../../models/cita'; 

@Injectable({
  providedIn: 'root'
})
export class CitasService {

  url: string;
  identity;
  token;

  constructor( public _http: HttpClient) {
    this.url = APIURL.url;
  }

  
getCitaPorProfesional(token: string, $id: number): Observable<any> {

  let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                  .set('Authorization', token);

  return this._http.get(this.url + 'getCitaPorProfesional/' + $id , {headers: headers});
}

getCitaPorProfesionalCalendar(token: string, $id: number): Observable<any> {

  let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                  .set('Authorization', token);

  return this._http.get(this.url + 'getCitaPorProfesionalFixed/' + $id , {headers: headers});
}

getCitaPorId(token: string, id: number): Observable<any> {
  let data = {
    id:id
  };
  let json = JSON.stringify(data);
  let params = 'json=' + json

  let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                  .set('Authorization', token);

  return this._http.post(this.url + 'getCita', params , {headers: headers});
}

getCitaPorProfesionalXSemanaAnteriorCalendar(token: string, id: number, dia: string): Observable<any> {
  let data = {
     id:id,
     dia:dia
  };
  let json = JSON.stringify(data);
  let params = 'json=' + json;
 
  let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                  .set('Authorization', token);
                                  
  return this._http.post(this.url + 'getCitaPorProfesionalXSemanaAnteriorFixed' , params, {headers: headers});
}

getCitaPorProfesionalXSemanaSiguienteCalendar(token: string, id: number, dia: string): Observable<any> {
  let data = {
     id:id,
     dia:dia
  };
  let json = JSON.stringify(data);
  let params = 'json=' + json;
  let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                  .set('Authorization', token);
                                  
  return this._http.post(this.url + 'getCitaPorProfesionalXSemanaSiguienteFixed' , params, {headers: headers});
}

  
getCitaPorProfesionalXSemanaAnterior(token: string, id: number, dia: string): Observable<any> {
  let data = {
     id:id,
     dia:dia
  };
  let json = JSON.stringify(data);
  let params = 'json=' + json;
 
  let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                  .set('Authorization', token);
                                  
  return this._http.post(this.url + 'getCitaPorProfesionalXSemanaAnterior' , params, {headers: headers});
}

getCitaPorProfesionalXSemanaSiguiente(token: string, id: number, dia: string): Observable<any> {
  let data = {
     id:id,
     dia:dia
  };
  let json = JSON.stringify(data);
  let params = 'json=' + json;
  let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                  .set('Authorization', token);
                                  
  return this._http.post(this.url + 'getCitaPorProfesionalXSemanaSiguiente' , params, {headers: headers});
}




getAtencionPorProfesional(token: string, $id: number): Observable<any> {

  let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                  .set('Authorization', token);

  return this._http.get(this.url + 'getAtencionPorProfesional/' + $id , {headers: headers});
}


updateStateCita(token: string, id: number, state:string): Observable<any> {
 
 let json = JSON.stringify(state);
 let params = 'json=' + json;
 
  let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                  .set('Authorization', token);
  return this._http.put(this.url + 'updateStateCita/'+ id ,params, {headers: headers});
}



updateStateAtencion(token: string, idcita:number, idatencion:number): Observable<any> {

  let data = {
    idCita: idcita,
    idAtencion:idatencion
 };

 let json = JSON.stringify(data);
 let params = 'json=' + json;
 
   let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                   .set('Authorization', token);
   return this._http.put(this.url + 'updateStateAtencion', params, {headers: headers});
}

addCita(token: string, cita: any, paciente: any): Observable<any> {
  let data = {
    cita: cita,
    paciente:paciente
  };
  let json = JSON.stringify(data);
  let params = 'json=' + json;
  let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                  .set('Authorization', token);
                                  
  return this._http.post(this.url + 'addCita' , params, {headers: headers});
}

getCitaEstadistica(token: string, $id: number): Observable<any> {

  let headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded')
                                  .set('Authorization', token);

  return this._http.get(this.url + 'getCitaEstadistica/' + $id , {headers: headers});
}


}

