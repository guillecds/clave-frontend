import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-back',
  templateUrl: './back.component.html',
  styles: []
})
export class BackComponent implements OnInit {
  @Input() url: string;
  constructor() { }

  ngOnInit() {
  }

}
