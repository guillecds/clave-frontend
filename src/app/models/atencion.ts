export class Antecedentes{

    public id: number;
    public fechalarga: string;
    public idcita: number;
    public idpac: number;
    public idprof: number;
 
    constructor(id:number, fechalarga:string, idpac: number,idcita:number, idprof:number)
    {
        this.id=id;
        this.fechalarga=fechalarga;
        this.idpac=idpac;
        this.idcita=idcita;    
        this.idprof=idprof;
    
    }
}   
