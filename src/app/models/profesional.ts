export class Profesional {

    public id: number;
    public apynom: string;
    public sexo: string;
    public tipodoc: string;
    public doc: string; //number??
    public mail: string;
    public matricula: number;
    public especialidad: number; 
    public tel: string;
    public cel: string;
    public nac: string; // están todas en distinto formato!!
    public direcc: string;
    public loc: string;
    public cp: number;
    public prov: string;
    public citaminutos: number;
    public citaconhora: number;
    public notas: string;
    public config: number;

    constructor(id:number, apynom:string, sexo:string,tipodoc:string,doc:string,mail:string, matricula:number, especialidad:number, tel:string, cel:string, 
        nac:string, direcc:string, loc:string,cp:number, prov:string, citaminutos:number, citaconhora:number, notas:string, config:number)
    {
        this.id=id;
        this.apynom=apynom;
        this.sexo=sexo;
        this.tipodoc=tipodoc;
        this.doc=doc;
        this.mail=mail;
        this.matricula=matricula;
        this.especialidad=especialidad;
        this.tel=tel;
        this.cel=cel;
        this.nac=nac;
        this.direcc=direcc;
        this.loc=loc;
        this.cp=cp;
        this.prov=prov;
        this.citaconhora=citaconhora;
        this.citaminutos=citaminutos;
        this.notas=notas;
        this.config=config;

    }
}
