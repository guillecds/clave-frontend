export class HistoriaClinica{

    public id: number;
    public idpac:number;
    public fecha: string;
    public motivo: string;
    public diagnostico:string;
    public tratamiento: string;
    public proximacita: string;

    constructor(id:number, idpac: number, fecha:string,motivo:string, diagnostico:string,tratamiento:string,proximacita:string)
    {
        this.id=id;
        this.idpac=idpac;
        this.fecha=fecha;
        this.motivo=motivo;
        this.diagnostico=diagnostico;
        this.tratamiento=tratamiento;
        this.proximacita=proximacita;
    }
}
