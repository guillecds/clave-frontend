export class Especialidad{

    public id: number;
    public especialidad: string;
  

    constructor(id:number, especialidad: string)
    {
        this.id=id;
        this.especialidad=especialidad;
    }
}
