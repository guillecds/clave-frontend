export class Cita{
    public id: number;
    public idprof: number;
    public idcons: number;
    public dia: string;
    public fechalarga: string;
    public idpac: number
    public estado: string;
    public notas: string;
    public tipodecita: string;


    constructor(id:number, idprof:number, idcons:number, dia:string, fechalarga:string, idpac:number, estado:string, notas:string, tipodecita:string)
    {

        this.id=id;
        this.idprof=idprof;
        this.idcons=idcons;
        this.dia=dia;
        this.fechalarga=fechalarga;
        this.idpac=idpac;
        this.estado=estado;
        this.notas=notas;
        this.tipodecita=tipodecita;
    }
}