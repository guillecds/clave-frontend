export class Consultorio{
    public id: number;
    public idprof: number;
    public domicilio: string;
    public localidad: string;
    public provincia: string;
    public telefono: string
    public nombre: string;
    public notas: string;


    constructor(id:number, idprof:number, domicilio:string, localidad:string, provincia:string, telefono:string, nombre:string, notas:string)
    {

        this.id=id;
        this.idprof=idprof;
        this.domicilio=domicilio;
        this.localidad=localidad;
        this.provincia=provincia;
        this.telefono=telefono;
        this.nombre=nombre;
        this.notas=notas;
     
    }
}