export class Usuario {

    public id: number;
    public sub: number;
    public username: string;
    public email: string;
    public nivel: string;
    public rol: string;
    public idprof: number;
    public password: string;
    public gettoken: string;

    constructor(id: number, sub: number, email: string, nivel: string, rol: string, idprof: number, password: string, username: string, estid: number) {
        this.id = id;
        this.sub = sub;
        this.email = email;
        this.nivel = nivel;
        this.rol = rol;
        this.idprof = idprof;
        this.password = password;
        this.gettoken = 'false';
        this.username = username;
    } 
}
