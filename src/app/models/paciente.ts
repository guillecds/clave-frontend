export class Paciente{

    public id: number;
    public apynom: string;
    public sexo: string;
    public tipodoc: string;
    public doc: string; //number??
    public obrasocial: number;
    public mail: string;
    public tel: string;
    public cel: string;
    public nac: string; // están todas en distinto formato!!
    public direcc: string;
    public loc: string;
    public ficha: string;
    public cp: number;
    public prov: string;
    public estciv: string;


    constructor(id:number, apynom:string, sexo:string,tipodoc:string,doc:string, obrasocial:number, mail:string, tel:string, cel:string, 
        nac:string, direcc:string, loc:string, ficha: string, cp:number, prov:string, estciv:string)
    {
        this.id=id;
        this.apynom=apynom;
        this.sexo=sexo;
        this.tipodoc=tipodoc;
        this.doc=doc;
        this.obrasocial=obrasocial;
        this.mail=mail;
        this.tel=tel;
        this.cel=cel;
        this.nac=nac;
        this.direcc=direcc;
        this.loc=loc;
        this.ficha=ficha;
        this.cp=cp;
        this.prov=prov;
        this.estciv=estciv;

    }
}
