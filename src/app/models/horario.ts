export class Horario{

    public id: number;
    public idprof: number;
    public dia: string;
    public horaini: number;
    public minini: number;
    public horalargai: number;
    public horafin: number;
    public minfin: number;
    public horalargaf: number;
    public idcons: number;

    constructor(id:number, idprof:number, dia: string,  horaini:number,  minini:number, horalargai:number, horafin:number, minfin: number,   horalargaf:number,  idcons:number )
    {
        this.id = id;
        this.idprof = idprof;
        this.dia = dia;
        this.horaini = horaini;
        this.minini = minini;
        this.horalargai = horalargai;
        this.horafin = horafin;
        this.minfin = minfin;
        this.horalargaf = horalargaf;
        this.idcons = idcons;
    }
}
