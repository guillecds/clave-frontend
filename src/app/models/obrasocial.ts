export class ObraSocial{

    public id: number;
    public nombre: string;
    public sigla: string;
    public descripcion: string;

    constructor(id:number, nombre: string, sigla:string, descripcion:string)
    {
        this.id=id;
        this.nombre=nombre;
        this.sigla=sigla;
        this.descripcion=descripcion;
    }
}
