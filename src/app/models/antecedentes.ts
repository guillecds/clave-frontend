export class Antecedentes{

    public id: number;
    public idpac: number;
    public creacion: string;
    public apers: string;
    public afam: string;
    public evol: string;
    public modificado: string;
  

    constructor(id:number, idpac: number,creacion: string, apers:string,afam:string,evol:string,modificado:string)
    {
        this.id=id;
        this.idpac=idpac;
        this.creacion=creacion;
        this.apers=apers;
        this.afam=afam;
        this.evol=evol;
        this.modificado=modificado;
    }
}
